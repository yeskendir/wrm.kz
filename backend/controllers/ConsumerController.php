<?php

namespace backend\controllers;

use common\models\ConsumerDescription;
use Yii;
use common\models\Consumer;
use common\models\ConsumerSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ConsumerController implements the CRUD actions for Consumer model.
 */
class ConsumerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Consumer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConsumerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Consumer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Consumer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Consumer();

        $model_consumer_description = new ConsumerDescription();

        if ($model->load(Yii::$app->request->post())) {

            if (empty($model->slug)) {
                $model->title = Yii::$app->request->post()["ConsumerDescription"][0]['title'];
            }

            if ($model->save()) {
                if (Yii::$app->request->post()["ConsumerDescription"]) {
                    foreach (Yii::$app->request->post()["ConsumerDescription"] AS $language_id => $description) {
                        $consumer_description = new ConsumerDescription();
                        $consumer_description->consumer_id = $model->id;
                        $consumer_description->language_id = $language_id;
                        $consumer_description->title = $description['title'];
                        $consumer_description->description = $description['description'];
                        $consumer_description->short_description = $description['short_description'];
                        $consumer_description->meta_title = $description['meta_title'];
                        $consumer_description->meta_description = $description['meta_description'];
                        $consumer_description->meta_keyword = $description['meta_keyword'];
                        $consumer_description->save();
                    }
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'model_consumer_description' => $model_consumer_description,
        ]);
    }

    /**
     * Updates an existing Consumer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model_consumer_description = new ConsumerDescription();

        if ($model->load(Yii::$app->request->post())) {

            if (empty($model->slug)) {
                $model->title = Yii::$app->request->post()["ConsumerDescription"][0]['title'];
            }

            if ($model->save()) {
                ConsumerDescription::deleteAll(array('consumer_id' => $model->id));

                foreach (Yii::$app->request->post()["ConsumerDescription"] AS $language_id => $description) {
                    $consumer_description = new ConsumerDescription();
                    $consumer_description->consumer_id = $model->id;
                    $consumer_description->language_id = $language_id;
                    $consumer_description->title = $description['title'];
                    $consumer_description->description = $description['description'];
                    $consumer_description->short_description = $description['short_description'];
                    $consumer_description->meta_title = $description['meta_title'];
                    $consumer_description->meta_description = $description['meta_description'];
                    $consumer_description->meta_keyword = $description['meta_keyword'];
                    $consumer_description->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'model_consumer_description' => $model_consumer_description,
        ]);
    }

    /**
     * Deletes an existing Consumer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        ConsumerDescription::deleteAll(array('consumer_id' => $id));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Consumer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Consumer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Consumer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

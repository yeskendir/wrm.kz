<?php

namespace backend\controllers;

use common\models\ArticleDescription;
use Yii;
use common\models\Article;
use common\models\ArticleSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();
        $model_article_description = new ArticleDescription();


        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {

                if (empty($model->slug)) {
                    $model->title = Yii::$app->request->post()["ArticleDescription"][0]['title'];
                }

                if (Yii::$app->request->post()["ArticleDescription"]) {
                    foreach (Yii::$app->request->post()["ArticleDescription"] AS $language_id => $description) {
                        $article_description = new ArticleDescription();
                        $article_description->article_id = $model->id;
                        $article_description->language_id = $language_id;
                        $article_description->title = $description['title'];
                        $article_description->description = $description['description'];
                        $article_description->short_description = $description['short_description'];
                        $article_description->meta_title = $description['meta_title'];
                        $article_description->meta_description = $description['meta_description'];
                        $article_description->meta_keyword = $description['meta_keyword'];
                        $article_description->save();
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'model_article_description' => $model_article_description,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model_article_description = new ArticleDescription();

        if ($model->load(Yii::$app->request->post())) {

            if (empty($model->slug)) {
                $model->title = Yii::$app->request->post()["ArticleDescription"][0]['title'];
            }

            if ($model->save()) {

                ArticleDescription::deleteAll(array('article_id' => $model->id));

                foreach (Yii::$app->request->post()["ArticleDescription"] AS $language_id => $description) {
                    $article_description = new ArticleDescription();
                    $article_description->article_id = $model->id;
                    $article_description->language_id = $language_id;
                    $article_description->title = $description['title'];
                    $article_description->description = $description['description'];
                    $article_description->short_description = $description['short_description'];
                    $article_description->meta_title = $description['meta_title'];
                    $article_description->meta_description = $description['meta_description'];
                    $article_description->meta_keyword = $description['meta_keyword'];

                    $article_description->save();
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'model_article_description' => $model_article_description
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        ArticleDescription::deleteAll(array('article_id' => $id));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

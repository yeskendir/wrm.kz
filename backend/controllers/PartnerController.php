<?php

namespace backend\controllers;

use common\models\PartnerDescription;
use Yii;
use common\models\Partner;
use common\models\PartnerSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PartnerController implements the CRUD actions for Partner model.
 */
class PartnerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Partner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PartnerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partner model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Partner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Partner();
        $model_partner_description = new PartnerDescription();

        if ($model->load(Yii::$app->request->post())) {

            if (empty($model->slug)) {
                $model->title = Yii::$app->request->post()["PartnerDescription"][0]['title'];
            }

            if ($model->save()) {

                if (Yii::$app->request->post()["PartnerDescription"]) {
                    foreach (Yii::$app->request->post()["PartnerDescription"] AS $language_id => $description) {
                        $partner_description = new PartnerDescription();
                        $partner_description->partner_id = $model->id;
                        $partner_description->language_id = $language_id;
                        $partner_description->title = $description['title'];
                        $partner_description->description = $description['description'];
                        $partner_description->short_description = $description['short_description'];
                        $partner_description->meta_title = $description['meta_title'];
                        $partner_description->meta_description = $description['meta_description'];
                        $partner_description->meta_keyword = $description['meta_keyword'];

                        $partner_description->save();
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
            'model_partner_description' => $model_partner_description,
        ]);
    }
    /**
     * Updates an existing Partner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model_partner_description = new PartnerDescription();
        if ($model->load(Yii::$app->request->post())) {

            if (empty($model->slug)) {
                $model->title = Yii::$app->request->post()["PartnerDescription"][0]['title'];
            }

            if ($model->save()) {

                PartnerDescription::deleteAll(array('partner_id' => $model->id));

                foreach (Yii::$app->request->post()["PartnerDescription"] AS $language_id => $description) {
                    $partner_description = new PartnerDescription();
                    $partner_description->partner_id = $model->id;
                    $partner_description->language_id = $language_id;
                    $partner_description->title = $description['title'];
                    $partner_description->description = $description['description'];
                    $partner_description->short_description = $description['short_description'];
                    $partner_description->meta_title = $description['meta_title'];
                    $partner_description->meta_description = $description['meta_description'];
                    $partner_description->meta_keyword = $description['meta_keyword'];

                    $partner_description->save();
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'model_partner_description' => $model_partner_description,
        ]);
    }

    /**
     * Deletes an existing Partner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        PartnerDescription::deleteAll(array('partner_id' => $id));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Partner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Partner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Partner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

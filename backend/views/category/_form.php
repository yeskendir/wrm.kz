<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'controller_id')->dropDownList(Yii::$app->params['controllers'], ['prompt' => '- Без категории -']) ?>

    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <? foreach (Yii::$app->params['languages'] AS $language_id => $language): ?>
                <li role="presentation" class="<?= ($language_id == 0) ? 'active' : '' ?>"><a
                            href="#language-<?= $language_id; ?>"
                            aria-controls="#language-<?= $language_id; ?>" role="tab"
                            data-toggle="tab"><span
                                class="text-uppercase"><?= $language ?></span></a></li>
            <? endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <? foreach (Yii::$app->params['languages'] AS $language_id => $language): ?>

                <div role="tabpanel" class="tab-pane <?= ($language_id == 0) ? 'active' : '' ?>"
                     id="language-<?= $language_id; ?>">

                    <? $category_description = \common\models\CategoryDescription::find()->where(['category_id' => $model->id, 'language_id' => $language_id])->one(); ?>
                    <? if ($category_description !== null): ?>
                        <?= $form->field($model_category_description, "[{$language_id}]name")->textInput(['maxlength' => true, 'value' => $category_description->name]) ?>
                    <? else: ?>
                        <?= $form->field($model_category_description, "[{$language_id}]name")->textInput(['maxlength' => true]) ?>
                    <? endif; ?>


                </div>
            <? endforeach; ?>
        </div>

    </div>


    <?= $form->field($model, 'sort_order')->input('number', ['min' => '0']) ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

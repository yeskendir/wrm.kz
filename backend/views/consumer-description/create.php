<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ConsumerDescription */

$this->title = Yii::t('app', 'Create Consumer Description');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Consumer Descriptions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consumer-description-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

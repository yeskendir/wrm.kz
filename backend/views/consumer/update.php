<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Consumer */

$this->title = Yii::t('app', 'Update Consumer: {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Consumers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="consumer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_consumer_description' => $model_consumer_description,
    ]) ?>

</div>

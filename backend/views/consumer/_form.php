<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use vova07\fileapi\Widget as FileAPI;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Consumer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="consumer-form">

    <?php $form = ActiveForm::begin(); ?>

    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <? foreach (Yii::$app->params['languages'] AS $language_id => $language): ?>
                <li role="presentation" class="<?= ($language_id == 0) ? 'active' : '' ?>"><a
                            href="#language-<?= $language_id; ?>"
                            aria-controls="#language-<?= $language_id; ?>" role="tab"
                            data-toggle="tab"><span
                                class="text-uppercase"><?= $language ?></span></a></li>
            <? endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <? foreach (Yii::$app->params['languages'] AS $language_id => $language): ?>

                <div role="tabpanel" class="tab-pane <?= ($language_id == 0) ? 'active' : '' ?>"
                     id="language-<?= $language_id; ?>">

                    <? $consumer_description = \common\models\ConsumerDescription::find()->where(['consumer_id' => $model->id, 'language_id' => $language_id])->one(); ?>
                    <? if ($consumer_description !== null): ?>

                        <?= $form->field($model_consumer_description, "[{$language_id}]title")->textInput(['maxlength' => true, 'value' => $consumer_description->title]) ?>

                        <?= $form->field($model_consumer_description, "[{$language_id}]description")->widget(Widget::className(), [
                            'options' => ['value' => $consumer_description->description],
                            'settings' => [
                                'lang' => 'ru',
                                'minHeight' => 150,
                                'imageUpload' => Url::to(['/site/image-upload']),
                                'imageManagerJson' => Url::to(['/site/images-get']),
                                'plugins' => [
                                    'imagemanager'
                                ]
                            ]
                        ]); ?>

                        <?= $form->field($model_consumer_description, "[{$language_id}]short_description")->textInput(['maxlength' => true, 'value' => $consumer_description->short_description]) ?>

                        <?= $form->field($model_consumer_description, "[{$language_id}]meta_title")->textInput(['maxlength' => true, 'value' => $consumer_description->meta_title]) ?>

                        <?= $form->field($model_consumer_description, "[{$language_id}]meta_description")->textInput(['maxlength' => true, 'value' => $consumer_description->meta_description]) ?>

                        <?= $form->field($model_consumer_description, "[{$language_id}]meta_keyword")->textInput(['maxlength' => true, 'value' => $consumer_description->meta_keyword]) ?>

                    <? else: ?>

                        <?= $form->field($model_consumer_description, "[{$language_id}]title")->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model_consumer_description, "[{$language_id}]description")->widget(Widget::className(), [
                            'settings' => [
                                'lang' => 'ru',
                                'minHeight' => 150,
                                'imageUpload' => Url::to(['/site/image-upload']),
                                'imageManagerJson' => Url::to(['/site/images-get']),
                                'plugins' => [
                                    'imagemanager'
                                ]
                            ]
                        ]); ?>
                        <?= $form->field($model_consumer_description, "[{$language_id}]short_description")->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model_consumer_description, "[{$language_id}]meta_title")->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model_consumer_description, "[{$language_id}]meta_description")->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model_consumer_description, "[{$language_id}]meta_keyword")->textInput(['maxlength' => true]) ?>

                    <? endif; ?>


                </div>
            <? endforeach; ?>
        </div>

    </div>

    <?= $form->field($model, 'sort_order')->input('number', ['min' => '0']) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

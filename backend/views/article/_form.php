<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\imperavi\Widget;
use vova07\fileapi\Widget as FileAPI;
use yii\helpers\Url;
use \common\models\CategoryDescription;
use \common\models\Category;

/* @var $this yii\web\View */
/* @var $model common\models\Article */
/* @var $form yii\widgets\ActiveForm */


$category_description = array();
$categories = Category::find()->where(['controller_id' => '0'])->all();
if ($categories) {
    foreach ($categories AS $category) {
        $category_info = CategoryDescription::find()->where(['category_id' => $category->id, 'language_id' => '0'])->one();
        $category_description[$category->id] = $category_info->name;
    }
}

?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')->dropDownList($category_description, ['prompt' => '- Без категории -']) ?>


    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <? foreach (Yii::$app->params['languages'] AS $language_id => $language): ?>
                <li role="presentation" class="<?= ($language_id == 0) ? 'active' : '' ?>"><a
                            href="#language-<?= $language_id; ?>"
                            aria-controls="#language-<?= $language_id; ?>" role="tab"
                            data-toggle="tab"><span
                                class="text-uppercase"><?= $language ?></span></a></li>
            <? endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <? foreach (Yii::$app->params['languages'] AS $language_id => $language): ?>

                <div role="tabpanel" class="tab-pane <?= ($language_id == 0) ? 'active' : '' ?>"
                     id="language-<?= $language_id; ?>">

                    <? $article_description = \common\models\ArticleDescription::find()->where(['article_id' => $model->id, 'language_id' => $language_id])->one(); ?>
                    <? if ($article_description !== null): ?>

                        <?= $form->field($model_article_description, "[{$language_id}]title")->textInput(['maxlength' => true, 'value' => $article_description->title]) ?>

                        <?= $form->field($model_article_description, "[{$language_id}]description")->widget(Widget::className(), [
                            'options' => ['value' => $article_description->description],
                            'settings' => [
                                'lang' => 'ru',
                                'minHeight' => 150,
                                'imageUpload' => Url::to(['/site/image-upload']),
                                'imageManagerJson' => Url::to(['/site/images-get']),
                                'plugins' => [
                                    'imagemanager'
                                ]
                            ]
                        ]); ?>

                        <?= $form->field($model_article_description, "[{$language_id}]short_description")->textInput(['maxlength' => true, 'value' => $article_description->short_description]) ?>

                        <?= $form->field($model_article_description, "[{$language_id}]meta_title")->textInput(['maxlength' => true, 'value' => $article_description->meta_title]) ?>

                        <?= $form->field($model_article_description, "[{$language_id}]meta_description")->textInput(['maxlength' => true, 'value' => $article_description->meta_description]) ?>

                        <?= $form->field($model_article_description, "[{$language_id}]meta_keyword")->textInput(['maxlength' => true, 'value' => $article_description->meta_keyword]) ?>

                    <? else: ?>

                        <?= $form->field($model_article_description, "[{$language_id}]title")->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model_article_description, "[{$language_id}]description")->widget(Widget::className(), [
                            'settings' => [
                                'lang' => 'ru',
                                'minHeight' => 150,
                                'imageUpload' => Url::to(['/site/image-upload']),
                                'imageManagerJson' => Url::to(['/site/images-get']),
                                'plugins' => [
                                    'imagemanager'
                                ]
                            ]
                        ]); ?>
                        <?= $form->field($model_article_description, "[{$language_id}]short_description")->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model_article_description, "[{$language_id}]meta_title")->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model_article_description, "[{$language_id}]meta_description")->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model_article_description, "[{$language_id}]meta_keyword")->textInput(['maxlength' => true]) ?>

                    <? endif; ?>


                </div>
            <? endforeach; ?>
        </div>

    </div>


    <?= $form->field($model, 'sort_order')->input('number', ['min' => '0']) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

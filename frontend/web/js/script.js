$(document).ready(function () {

    var contact_form = $("#contact-form");
    contact_form.submit(function () {
        var form = $(this);

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function (response) {
                var alert = $('.call .alert');
                if (response.success) {
                    if (alert.hasClass('alert-danger')) {
                        alert.removeClass('alert-danger');
                    }
                    alert.addClass('alert-success');
                    alert.html(response.success);

                } else if (response.error) {
                    if (alert.hasClass('alert-success')) {
                        alert.removeClass('alert-success');
                    }

                    alert.addClass('alert-danger');
                    alert.html(response.error);
                }

                var contact_form = $("#contact-form");

                if (contact_form.find(".form-group").hasClass('has-error')) {
                    contact_form.find(".form-group").removeClass('has-error');
                    contact_form.find('.help-block').html('');
                }
                contact_form.find(".form-group").addClass('has-success');


                if (response.validate) {
                    $.each(response.validate, function (id, text) {
                        var form_group = $('#' + id).parents('.form-group');

                        if (form_group.hasClass('has-success')) {
                            form_group.removeClass('has-success');
                        }

                        form_group.addClass('has-error');
                        form_group.find('.help-block').html(text);
                    });
                } else {
                    contact_form.find(".form-group").removeClass('has-success');
                }
            }
        });
        return false;
    });

    if ($('.info-menu-link').length) {
        var current = location.search;
        $('.info-menu-link').each(function () {
            var $this = $(this);
            // if the current path is like this link, make it active
            console.log($this.attr('href').indexOf(current));
            if ($this.attr('href').indexOf(current) !== -1) {
                $this.parents('.info-menu-item').addClass('active');
            }
        });
    }

});
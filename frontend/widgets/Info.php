<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;


class Info extends Widget
{

    public function init()
    {
        parent::init();


        echo $this->render('info');

    }

}

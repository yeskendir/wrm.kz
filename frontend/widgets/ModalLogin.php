<?php

namespace frontend\widgets;

use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\SignupForm;
use Yii;
use yii\base\Widget;


class ModalLogin extends Widget
{

    public function init()
    {
        parent::init();

        $login_form = new LoginForm();

        $resetForm = new PasswordResetRequestForm();

        $signup_form = new SignupForm();

        echo $this->render('modalLogin', [
            'login_form' => $login_form,
            'resetForm' => $resetForm,
            'signup_form' => $signup_form
        ]);

    }

}

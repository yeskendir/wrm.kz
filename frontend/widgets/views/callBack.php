<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


/* @var $this yii\web\View
 * @var $news
 * @var $width
 * @var $height
 * @var $color
 */
?>

<div class="call">
    <div class="container text-white">
        <div class="alert"></div>
        <div class="call-title text-uppercase text-center">обратная связь</div>
        <div class="row">

            <?php $form = ActiveForm::begin(['id' => 'contact-form', 'action' => ['site/contact']]); ?>

            <div class="col-sm-6 call-left">
                <?= $form->field($contact_form, 'name')->label('Ф.И.О.:') ?>

                <?= $form->field($contact_form, 'subject')->label('Телефон:') ?>

                <?= $form->field($contact_form, 'email')->label('Email:') ?>
            </div>

            <div class="col-sm-6 call-right">
                <?= $form->field($contact_form, 'body')->textarea(['rows' => 6])->label('Комментарий:') ?>
            </div>

            <div class="col-xs-12 text-center">
                <button type="submit" name="contact-button" class="btn btn-info btn-call-back">Отправить</button>
            </div>

            <?php ActiveForm::end(); ?>


        </div>
    </div>
</div>
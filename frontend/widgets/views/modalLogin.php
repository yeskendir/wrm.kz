<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/* @var $this yii\web\View
 * @var $news
 * @var $width
 * @var $height
 * @var $color
 */
?>

<div class="modal fade modal-custom modal-signup" id="signUpModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="login">
                        <div class="modal-signup-logo text-center text-uppercase">
                            <img src="/img/company-logo.png" alt="ТОО «Водные ресурсы-Маркетинг»"
                                 class="img-responsive modal-signup-logo-img">
                            <span class="modal-signup-logo-text">Вход в систему</span>
                        </div>

                        <?php $form = ActiveForm::begin([
                            'id' => 'modal-login-form',
                            'action' => ['site/login']
                        ]); ?>

                        <?= $form->field($login_form, 'username')->textInput(['autofocus' => true])->input('text', ['placeholder' => "Логин"])->label(false) ?>

                        <?= $form->field($login_form, 'password')->passwordInput()->input('password', ['placeholder' => "Пароль"])->label(false) ?>

                        <?= $form->field($login_form, 'rememberMe')->checkbox(); ?>

                        <div class="form-group form-button">
                            <button type="submit" class="btn btn-primary" name="login-button">Войти</button>
                            <div class="modal-signup-auth">
                                <a href="#resetPassword" aria-controls="resetPassword" role="tab"
                                   data-toggle="tab" class="modal-signup-auth-link">Забыли логин или пароль?</a>
                                <a href="#signup" aria-controls="signup" role="tab" data-toggle="tab"
                                   class="modal-signup-auth-link">Зарегистрироваться</a>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="signup">
                        <div class="modal-signup-logo text-center text-uppercase">
                            <img src="/img/company-logo.png" alt="ТОО «Водные ресурсы-Маркетинг»"
                                 class="img-responsive modal-signup-logo-img">
                            <span class="modal-signup-logo-text">Регистрация</span>
                        </div>

                        <?php $form = ActiveForm::begin(['id' => 'modal-form-signup', 'action' => ['site/signup']]); ?>

                        <?= $form->field($signup_form, 'username')->textInput(['autofocus' => true])->input('text', ['placeholder' => "Логин"])->label(false) ?>

                        <?= $form->field($signup_form, 'email')->input('email', ['placeholder' => "Email"])->label(false) ?>

                        <?= $form->field($signup_form, 'password')->passwordInput()->input('password', ['placeholder' => "Пароль"])->label(false) ?>

                        <div class="form-group form-button">
                            <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
                            <div class="modal-signup-auth">
                                <a href="#login" aria-controls="login" role="tab" data-toggle="tab"
                                   class="modal-signup-auth-link">Aвторизоваться</a>
                                <a href="#resetPassword" aria-controls="resetPassword" role="tab"
                                   data-toggle="tab" class="modal-signup-auth-link">Забыли логин или пароль?</a>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="resetPassword">
                        <div class="modal-signup-logo text-center text-uppercase">
                            <img src="/img/company-logo.png" alt="ТОО «Водные ресурсы-Маркетинг»"
                                 class="img-responsive modal-signup-logo-img">
                            <span class="modal-signup-logo-text">Забыли пароль?</span>
                        </div>

                        <?php $form = ActiveForm::begin(['id' => 'modal-request-password-reset-form', 'action' => ['site/request-password-reset']]); ?>

                        <?= $form->field($resetForm, 'email')->input('email', ['placeholder' => "Ваш E-Mail"])->label(false) ?>


                        <div class="form-group form-button">
                            <button type="submit" class="btn btn-primary">Отправить</button>
                            <div class="modal-signup-auth">
                                <a href="#login" aria-controls="login" role="tab" data-toggle="tab"
                                   class="modal-signup-auth-link">Aвторизоваться</a>
                                <a href="#signup" aria-controls="signup" role="tab" data-toggle="tab"
                                   class="modal-signup-auth-link">Зарегистрироваться</a>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $('.js-modal-login').click(function (e) {
            e.preventDefault();
            $('#signUpModal').modal('show');
            $('a[href="#login"]').tab('show');
        });

        $('.js-modal-signup').click(function (e) {
            e.preventDefault();
            $('#signUpModal').modal('show');
            $('.modal-signup-auth a[href="#signup"]').tab('show');
        });

        var login_form = $("#modal-login-form");
        login_form.submit(function () {
            var form = $(this);

            $.ajax({
                url: form.attr('action'),
                type: 'post',
                data: form.serialize(),
                success: function (response) {
                    if (response.status == 'success') {
                        location.reload();
                    }

                    var login_form = $("#modal-login-form");

                    if (login_form.find(".form-group").hasClass('has-error')) {
                        login_form.find(".form-group").removeClass('has-error');
                        login_form.find('.help-block').html('');
                    }
                    login_form.find(".form-group").addClass('has-success');


                    if (response.validate) {
                        $.each(response.validate, function (id, text) {
                            var form_group = $('#' + id).parents('.form-group');

                            if (form_group.hasClass('has-success')) {
                                form_group.removeClass('has-success');
                            }

                            form_group.addClass('has-error');
                            form_group.find('.help-block').html(text);
                        });
                    } else {
                        login_form.find(".form-group").removeClass('has-success');
                    }
                }
            });
            return false;
        });

        var reset_form = $("#modal-request-password-reset-form");
        reset_form.submit(function () {
            var form = $(this);

            $.ajax({
                url: form.attr('action'),
                type: 'post',
                data: form.serialize(),
                success: function (response) {
                    if (response.status == 'success') {
                        location.reload();
                    }

                    var reset_form = $("#modal-request-password-reset-form");

                    if (reset_form.find(".form-group").hasClass('has-error')) {
                        reset_form.find(".form-group").removeClass('has-error');
                        reset_form.find('.help-block').html('');
                    }
                    reset_form.find(".form-group").addClass('has-success');

                    if (response.validate) {
                        $.each(response.validate, function (id, text) {
                            var form_group = $('#' + id).parents('.form-group');

                            if (form_group.hasClass('has-success')) {
                                form_group.removeClass('has-success');
                            }

                            form_group.addClass('has-error');
                            form_group.find('.help-block').html(text);
                        });
                    }
                }
            });
            return false;
        });

        var signup_form = $("#modal-form-signup");
        signup_form.submit(function () {
            var form = $(this);

            $.ajax({
                url: form.attr('action'),
                type: 'post',
                data: form.serialize(),
                success: function (response) {
                    if (response.status == 'success') {
                        location.reload();
                    }

                    var signup_form = $("#modal-form-signup");

                    if (signup_form.find(".form-group").hasClass('has-error')) {
                        signup_form.find(".form-group").removeClass('has-error');
                        signup_form.find('.help-block').html('');
                    }
                    signup_form.find(".form-group").addClass('has-success');

                    if (response.validate) {
                        $.each(response.validate, function (id, text) {
                            var form_group = $('#' + id).parents('.form-group');

                            if (form_group.hasClass('has-success')) {
                                form_group.removeClass('has-success');
                            }

                            form_group.addClass('has-error');
                            form_group.find('.help-block').html(text);
                        });
                    }
                }
            });
            return false;
        });

    });
</script>
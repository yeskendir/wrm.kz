<?php

namespace frontend\widgets;

use frontend\models\ContactForm;
use Yii;
use yii\base\Widget;


class CallBack extends Widget
{

    public function init()
    {
        parent::init();

        $contact_form = new ContactForm();

        echo $this->render('callBack', [
            'contact_form' => $contact_form
        ]);

    }

}

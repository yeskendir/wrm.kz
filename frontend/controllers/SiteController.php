<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */

    public function actionGallery()
    {
        return $this->render('gallery');
    }

    /**
     * @inheritdoc
     */
    public function actionNews()
    {
        return $this->render('news');
    }

    /**
     * @inheritdoc
     */
    public function actionAnswer()
    {
        return $this->render('answer');
    }

    /**
     * @inheritdoc
     */
    public function actionArchive()
    {
        return $this->render('archive');
    }

    /**
     * @inheritdoc
     */
    public function actionArticle()
    {
        return $this->render('article');
    }

    /**
     * @inheritdoc
     */
    public function actionHistory()
    {
        return $this->render('history');
    }

    /**
     * @inheritdoc
     */
    public function actionManagement()
    {
        return $this->render('management');
    }

    /**
     * @inheritdoc
     */
    public function actionQuiz()
    {
        return $this->render('quiz');
    }


    /**
     * @inheritdoc
     */
    public function actionRate()
    {
        return $this->render('rate');
    }

    /**
     * @inheritdoc
     */
    public function actionReport()
    {
        return $this->render('report');
    }

    /**
     * @inheritdoc
     */
    public function actionStructure()
    {
        return $this->render('structure');
    }

    /**
     * @inheritdoc
     */
    public function actionVideo()
    {
        return $this->render('video');
    }

    /**
     * @inheritdoc
     */
    public function actionZakupki()
    {
        return $this->render('zakupki');
    }
    /**
     * @inheritdoc
     */
    public function actionSManagement()
    {
        return $this->render('smanagement');
    }

    /**
     * @inheritdoc
     */
    public function actionCabinetData()
    {
        return $this->render('cabinetData');
    }

    /**
     * @inheritdoc
     */
    public function actionCabinetIndications()
    {
        return $this->render('cabinetIndications');
    }

    /**
     * @inheritdoc
     */
    public function actionCabinetPay()
    {
        return $this->render('cabinetPay');
    }

    /**
     * @inheritdoc
     */
    public function actionCabinetQuotation()
    {
        return $this->render('cabinetQuotation');
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $model = new LoginForm();


        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (!Yii::$app->user->isGuest) {
                return array('status' => 'success');
            }

            $validate = ActiveForm::validate($model);
            if ($validate) {
                return array('validate' => $validate);
            }

            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return array('status' => 'success');
            } else {
                return array('status' => 'error');
            }

        }

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


            $validate = ActiveForm::validate($model);
            if ($validate) {
                return array('validate' => $validate);
            }

            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                return array('success' => 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                return array('error' => 'There was an error sending your message.');
            }

        }


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact1', [
                'model' => $model,
            ]);
        }

        return $this->render('contact1');
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionTest()
    {
        return $this->render('test');
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionPartners()
    {
        return $this->render('partners');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


            $validate = ActiveForm::validate($model);
            if ($validate) {
                return array('validate' => $validate);
            }

            if ($model->load(Yii::$app->request->post())) {
                if ($user = $model->signup()) {
                    if (Yii::$app->getUser()->login($user)) {
                        return array('status' => 'success');
                    }
                }
            }

        }

        if ($model->load(Yii::$app->request->post())) {

            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();


        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $validate = ActiveForm::validate($model);
            if ($validate) {
                return array('validate' => $validate);
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->sendEmail()) {
                    Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                } else {
                    Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
                }
                return array('status' => 'success');
            }

        }


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}

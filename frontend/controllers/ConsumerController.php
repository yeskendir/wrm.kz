<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\CategoryDescription;
use common\models\Consumer;
use common\models\ConsumerDescription;
use common\models\Partner;
use common\models\PartnerDescription;
use yii\web\NotFoundHttpException;

class ConsumerController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $data = array();

        $consumers = Consumer::find()->where(array('status' => '1'))->orderBy(array('sort_order' => SORT_DESC))->all();
        foreach ($consumers AS $consumer) {
            $consumer_info = ConsumerDescription::find()->where(array('consumer_id' => $consumer->id, 'language_id' => LANGUAGE_ID))->one();
            $data['consumer'][] = $consumer_info;
        }

        return $this->render('index', [
            'data' => $data,
        ]);
    }

//    public function actionIndex($slug = '')
//    {
//        $query = Partner::find()->where(['is_published' => '1'])->orderBy('created DESC');
//
//        if($slug){
//            $category = $this->findCategory($slug);
//            $query->andWhere(['category_id' => $category->id]);
//        }
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//            'pagination' => [
//                'pageSize' => 6,
//            ],
//        ]);
//
//        $categories = Category::find()->where(['model_name' => 'partner'])->all();
//
//        return $this->render('index', [
//            'dataProvider' => $dataProvider,
//            'categories' => $categories,
//        ]);
//    }


    public function actionView($id, $slug = false)
    {
        $data = array();

        $consumers = Consumer::find()->where(array('status' => '1'))->orderBy(array('sort_order' => SORT_DESC))->all();
        foreach ($consumers AS $consumer) {
            $consumer_info = ConsumerDescription::find()->where(array('consumer_id' => $consumer->id, 'language_id' => LANGUAGE_ID))->one();
            $data['consumer'][] = $consumer_info;
        }
        $model = $this->findConsumer($id, $slug = false);

        return $this->render('view', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    protected function findConsumer($id, $slug = false)
    {
        $consumer = Consumer::find()->where(array('status' => '1', 'id' => $id))->one();
        if ($consumer) {
            $consumer_info = ConsumerDescription::find()->where(array('consumer_id' => $consumer->id, 'language_id' => LANGUAGE_ID))->one();
            return $consumer_info;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findCategory($slug)
    {
        if (($model = Category::find()->where(['slug' => $slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

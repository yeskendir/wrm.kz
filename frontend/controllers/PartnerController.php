<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\CategoryDescription;
use common\models\Partner;
use common\models\PartnerDescription;
use yii\web\NotFoundHttpException;

class PartnerController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $data = array();

        $categories = Category::find()->where(['status' => '1', 'controller_id' => Partner::$controller_id])->orderBy(array('sort_order' => SORT_DESC))->all();
        foreach ($categories AS $i => $category) {
            $categories_info = CategoryDescription::find()->where(array('category_id' => $category->id, 'language_id' => LANGUAGE_ID))->one();
            if ($categories_info) {
                $data[$i]['category'] = $categories_info;
                $partners = Partner::find()->where(array('status' => '1', 'category_id' => $category->id))->orderBy(array('sort_order' => SORT_DESC))->all();
                foreach ($partners AS $partner) {
                    $partner_info = PartnerDescription::find()->where(array('partner_id' => $partner->id, 'language_id' => LANGUAGE_ID))->one();
                    $data[$i]['partner'][] = $partner_info;
                }
            }
        }

        return $this->render('index', [
            'data' => $data,
        ]);
    }

//    public function actionIndex($slug = '')
//    {
//        $query = Partner::find()->where(['is_published' => '1'])->orderBy('created DESC');
//
//        if($slug){
//            $category = $this->findCategory($slug);
//            $query->andWhere(['category_id' => $category->id]);
//        }
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//            'pagination' => [
//                'pageSize' => 6,
//            ],
//        ]);
//
//        $categories = Category::find()->where(['model_name' => 'partner'])->all();
//
//        return $this->render('index', [
//            'dataProvider' => $dataProvider,
//            'categories' => $categories,
//        ]);
//    }


    public function actionView($id, $slug = false)
    {

        $model = $this->findPartner($id, $slug = false);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    protected function findPartner($id, $slug = false)
    {
        $partner = Partner::find()->where(array('status' => '1', 'id' => $id))->one();
        if ($partner) {
            $partner_info = PartnerDescription::find()->where(array('partner_id' => $partner->id, 'language_id' => LANGUAGE_ID))->one();
            return $partner_info;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findCategory($slug)
    {
        if (($model = Category::find()->where(['slug' => $slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

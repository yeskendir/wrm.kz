<?php

use frontend\widgets\ProductWidget;
use yii\helpers\Url;

/** @var $model \common\models\Article */
$this->title = $model->meta_title ? $model->meta_title : $model->title;
$this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keyword]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description]);

$this->params['breadcrumbs'][] = $model->title;
?>

<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="/" class="navigation-breadcrumb-link">Главная</a></li>
            <li><a href="<?= Url::toRoute(['consumer/index']); ?>" class="navigation-breadcrumb-link">Потребителю</a></li>
            <li class="active"><?= $model->title; ?></li>
        </ol>
        <h1 class="text-uppercase navigation-title"><?= $model->title; ?></h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('_navCompany', [
                    'title' => $model->title
                ]) ?>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <div class="text-uppercase report-title text-center"><?= $model->title; ?></div>
                            <div class="report-body">
                                <?= $model->description; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/history.css">


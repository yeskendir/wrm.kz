<?

use \yii\helpers\Url;

?>

<div class="info-menu">
    <div class="info-menu-title text-uppercase"><?= $title; ?></div>
    <div class="info-menu-list">
        <div class="info-menu-item">
            <a href="<?= Url::toRoute(['site/answer']); ?>" class="info-menu-link">Вопросы-ответы</a>
        </div>
        <? if (isset($data['consumer'])): ?>
            <? foreach ($data['consumer'] AS $consumer): ?>
                <div class="info-menu-item">
                    <a href="<?= Url::toRoute(['consumer/view', 'id' => $consumer['consumer_id']]); ?>" class="info-menu-link"><?= $consumer['title']; ?></a>
                </div>
            <? endforeach; ?>
        <? endif; ?>
    </div>
</div>
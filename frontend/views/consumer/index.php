<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Потребителю';

$this->params['breadcrumbs'][] = 'Потребителю';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'Потребителю']);
$this->registerMetaTag(['name' => 'description', 'content' => 'Потребителю']);

?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active"><?= $this->title ?></li>
        </ol>
        <h1 class="text-uppercase navigation-title"><?= $this->title ?></h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('_navCompany', [
                    'title' => 'Потребителю',
                    'data' => $data
                ]); ?>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <div class="text-uppercase report-title text-center">ПОТРЕБИТЕЛЮ</div>
                            <div class="report-body">

                                <? if ($data['consumer']): ?>
                                    <ul class="list-group">
                                        <? foreach ($data['consumer'] AS $consumer): ?>
                                            <li class="list-group-item">
                                                <a href="<?= Url::toRoute(['consumer/view', 'id' => $consumer['consumer_id']]); ?>"><?= $consumer['title']; ?></a>
                                            </li>
                                        <? endforeach; ?>
                                    </ul>
                                <? endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/history.css">

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?= \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
    </div>
</div>
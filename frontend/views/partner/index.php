<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Статьи';

$this->params['breadcrumbs'][] = 'Партнеры';
$this->registerMetaTag(['name' => 'keywords', 'content' => 'Партнеры']);
$this->registerMetaTag(['name' => 'description', 'content' => 'Партнеры']);

?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Партнеры</li>
        </ol>
        <h1 class="text-uppercase navigation-title">ПАРТНЕРЫ</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('_navCompany', [
                    'title' => 'Партнеры'
                ]); ?>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <div class="text-uppercase report-title text-center">ПАРТНЕРЫ
                            </div>
                            <div class="report-body">
                                <? if (isset($data) && is_array($data)): ?>
                                    <? foreach ($data AS $id => $item): ?>
                                        <div class="panel panel-default">

                                            <div class="panel-heading" role="tab" id="heading-<?= $id; ?>">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse-<?= $id; ?>" aria-expanded="true"
                                                       aria-controls="collapse-<?= $id; ?>">
                                                        <?= $item['category']['name']; ?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <? if (isset($item['partner'])): ?>
                                                <div id="collapse-<?= $id; ?>" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="heading-<?= $id; ?>">
                                                    <div class="panel-body">

                                                        <ul class="list-group">
                                                            <? foreach ($item['partner'] AS $partner): ?>
                                                                <li class="list-group-item">
                                                                    <a href="<?= Url::toRoute(['partner/view', 'id' => $partner['partner_id']]); ?>"><?= $partner['title']; ?></a>
                                                                </li>
                                                            <? endforeach; ?>
                                                        </ul>

                                                    </div>
                                                </div>
                                            <? endif; ?>
                                        </div>
                                    <? endforeach; ?>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/history.css">

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?= \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
    </div>
</div>
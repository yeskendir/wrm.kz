<?

use \yii\helpers\Url;

?>

<div class="info-menu">
    <div class="info-menu-title text-uppercase"><?= $title; ?></div>
    <div class="info-menu-list">
        <div class="info-menu-item">
            <a href="<?= Url::toRoute(['site/management']); ?>" class="info-menu-link">Руководство</a>
        </div>
        <div class="info-menu-item">
            <a href="<?= Url::toRoute(['site/structure']); ?>" class="info-menu-link">Структура</a>
        </div>
        <div class="info-menu-item">
            <a href="<?= Url::toRoute(['site/history']); ?>" class="info-menu-link">История</a>
        </div>
        <div class="info-menu-item">
            <a href="<?= Url::toRoute(['site/s-management']); ?>" class="info-menu-link">Интегрированная система
                менеджмента </a>
        </div>
        <div class="info-menu-item">
            <a href="<?= Url::toRoute(['site/partners']); ?>" class="info-menu-link">Партнеры</a>
        </div>
    </div>
</div>
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>
<?php $this->beginBody() ?>

<header class="header">
    <div class="header-top">
        <div class="container">
            <ul class="nav nav-pills header-nav-pills">
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle header-top-link" data-toggle="dropdown" role="button"
                       aria-haspopup="true"
                       aria-expanded="false">
                        О компании <span class="caret-custom"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= Url::toRoute(['site/management']); ?>">Руководство</a></li>
                        <li><a href="<?= Url::toRoute(['site/structure']); ?>">Структура</a></li>
                        <li><a href="<?= Url::toRoute(['site/history']); ?>">История</a></li>
                        <li><a href="<?= Url::toRoute(['site/s-management']); ?>">Интегрированная система менеджмента</a></li>
                        <li><a href="<?= Url::toRoute(['partner/index']); ?>">Партнеры</a></li>
                    </ul>
                </li>
                <li><a href="<?= Url::toRoute(['site/zakupki']); ?>" class="header-top-link">закупки</a></li>
                <li><a href="<?= Url::toRoute(['site/contact']); ?>" class="header-top-link">Контакты</a></li>
            </ul>
            <div class="header-top-search">
                <div class="search-input-group">
                    <input class="form-control" placeholder="Поиск на сайте">
                    <button class="btn">
                        <img src="/img/icon-search.png" alt="Пойск" class="img-responsive">
                    </button>
                </div>
            </div>
            <ul class="nav nav-pills header-nav-tools">
                <li class="tools-item">
                    <a href="#" class="tools-link">
                        <img src="/img/icon-share.png" alt="Поделиться" class="img-responsive">
                    </a>
                </li>
                <li class="tools-item">
                    <a href="#" class="tools-link">
                        <img src="/img/icon-map.png" alt="Карта сайта" class="img-responsive">
                    </a>
                </li>
                <li role="presentation" class="dropdown dropdown-lang">
                    <a class="dropdown-toggle text-white" data-toggle="dropdown" href="#"
                       role="button"
                       aria-haspopup="true" aria-expanded="false">
                        Рус<span class="caret-custom"></span>
                    </a>
                    <ul class="dropdown-menu text-white">
                        <li><a href="#">Каз</a></li>
                    </ul>
                </li>
                <? if (Yii::$app->user->isGuest): ?>
                    <li class="tools-item tools-cabinet">
                        <a href="<?= Url::toRoute(['/site/login']); ?>"
                           class="text-white js-modal-login">
                            <img src="/img/icon-user.png"
                                 alt="Личный кабинет"
                                 class="img-responsive tools-cabinet-img">
                        <span class="hidden-sm tools-cabinet-title">
                                Войти
                        </span> <span class="clearfix"></span>
                        </a>
                    </li>
                <? else: ?>
                    <li role="presentation" class="dropdown tools-item tools-cabinet">
                        <a class="dropdown-toggle text-white" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <img src="/img/icon-user.png"
                                 alt="Личный кабинет"
                                 class="img-responsive tools-cabinet-img">
                            <span class="hidden-sm tools-cabinet-title">Личный кабинет</span>
                            <span class="clearfix"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Url::toRoute(['/site/cabinet-data']); ?>">Данные</a></li>
                            <li><a href="<?= Url::toRoute(['/site/cabinet-indications']); ?>">Оплата за воду</a></li>
                            <li><a href="<?= Url::toRoute(['/site/cabinet-pay']); ?>">Показания учета</a></li>
                            <li><a href="<?= Url::toRoute(['/site/cabinet-quotation']); ?>">Распечатать квитанцию</a></li>
                            <li><a href="<?= Url::toRoute(['/site/logout']); ?>">Выход</a></li>
                        </ul>
                    </li>
                <? endif; ?>
            </ul>
        </div>
    </div>
    <div class="header-main">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only"><img src="/img/logo.png" alt="Logo" class="img-responsive"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img src="/img/logo.png" alt="Logo" class="img-responsive">
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="clearfix hidden-lg hidden-md hidden-xs"></div>
                    <ul class="nav navbar-nav">
                        <li><a href="<?= Url::toRoute(['site/report']); ?>">Отчеты</a></li>
                        <li><a href="<?= Url::toRoute(['consumer/index']); ?>">Потребителю</a></li>
                        <li><a href="<?= Url::toRoute(['site/gallery']); ?>">Галерея</a></li>
                        <li><a href="<?= Url::toRoute(['site/news']); ?>">Новости</a></li>
                    </ul>
                    <div class="navbar-right navbar-info">
                        <div class="navbar-info-time">звоните нам: пн. - сб. 9:00 – 18:00</div>
                        <div class="navbar-info-phone">+7 (7252) 321-275</div>
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>
</header>

<div id="content">
    <div class="container">
        <? Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
    </div>
    <?= $content ?>
</div>


<footer class="footer">
    <div class="footer-company bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-company-item">
                        <div class="footer-company-left">
                            <img src="/img/icon-kz.png" alt="Официальный сайт президента Республики казахстан"
                                 class="img-responsive footer-company-img">
                        </div>
                        <div class="footer-company-right">
                            <div class="footer-company-name">Официальный сайт президента Республики казахстан</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-company-item">
                        <div class="footer-company-left">
                            <img src="/img/icon-egov.png" alt="Государственные услуги и информация онлайн"
                                 class="img-responsive footer-company-img">

                        </div>

                        <div class="footer-company-right">
                            <div class="footer-company-name">Государственные услуги и информация онлайн</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-company-item">
                        <div class="footer-company-left">
                            <img src="/img/icon-hz.png" alt="План нации - 100 конкретных шагов"
                                 class="img-responsive footer-company-img">
                        </div>

                        <div class="footer-company-right">
                            <div class="footer-company-name">План нации - 100 конкретных шагов</div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-company-item">
                        <div class="footer-company-left">
                            <img src="/img/icon-shymkent.png" alt="Официальный интернет ресурс Акимата города Шымкент"
                                 class="img-responsive footer-company-img">
                        </div>
                        <div class="footer-company-right">
                            <div class="footer-company-name">Официальный интернет ресурс Акимата города Шымкент</div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>

    </div>
    <div class="footer-info">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="footer-nav">
                        <div class="footer-nav-list">
                            <div class="footer-nav-item"><a href="<?= Url::toRoute(['site/history']); ?>"
                                                            class="footer-nav-link">О компании</a></div>
                            <div class="footer-nav-item"><a href="<?= Url::toRoute(['site/quiz']); ?>"
                                                            class="footer-nav-link">Потребителю</a></div>
                            <div class="footer-nav-item"><a href="<?= Url::toRoute(['site/gallery']); ?>"
                                                            class="footer-nav-link">Галерея</a></div>
                        </div>
                        <div class="footer-nav-list">
                            <div class="footer-nav-item"><a href="<?= Url::toRoute(['site/report']); ?>"
                                                            class="footer-nav-link">Отчеты</a></div>
                            <div class="footer-nav-item"><a href="<?= Url::toRoute(['site/zakupki']); ?>"
                                                            class="footer-nav-link">Закупки</a></div>
                            <div class="footer-nav-item"><a href="<?= Url::toRoute(['site/news']); ?>"
                                                            class="footer-nav-link">Новости</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="social text-center">
                        <a href="#" class="social-link">
                            <img src="/img/icon-vk.png" alt="vk.com">
                        </a>
                        <a href="#" class="social-link">
                            <img src="/img/icon-tw.png" alt="tw.com">
                        </a>
                        <a href="#" class="social-link">
                            <img src="/img/icon-inst.png" alt="inst.com">
                        </a>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="address">
                                <div class="address-text">Республика Казахстан, г. Шымкент,<br> Адрес: г.Шымкент, ул.
                                    Г.Орманова 17
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="call-phone">
                                <div class="call-phone-time text-uppercase">звоните нам: пн. - СБ. 9:00 – 18:00</div>
                                <a class="call-phone-link" href="tel:77252321275">+7 (7252) 321-275</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row footer-bottom">
                <div class="col-md-5">
                    <div class="footer-bottom-site">wrm.kz © WMR,2013 - <?= date('Y') ?> гг.</div>
                </div>
                <div class="col-md-2">
                    <img src="/img/footer-logo.png" alt="Logo" class="img-responsive center-block">
                </div>
                <div class="col-md-5">
                    <a href="http://mediarocket.kz" class="footer-created-link">Разработано в «Media Rocket»</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php $this->endBody() ?>

<!--region Modal Sign up -->
<?= \frontend\widgets\ModalLogin::widget(); ?>
<!--endregion Modal Sign up -->

</body>
</html>
<?php $this->endPage() ?>

<?php

/* @var $this yii\web\View */

$this->title = 'Rate';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Структура</li>
        </ol>
        <h1 class="text-uppercase navigation-title">СТРУКТУРА</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="info-menu">
                    <div class="info-menu-title text-uppercase">потребителю</div>
                    <div class="info-menu-list">
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Вопросы-ответы</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Информация для абонентов</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Отчеты перед потребителем</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Тарифы</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Опрос потребителей</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Запрос задолженности</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <p>В соответствии с приказом Департамента Комитета по регулированию естественных монополий и
                                защите конкуренции
                                Министерства национальной экономики Республики Казахстан по ЮКО № 179-ОД от 16 июня 2016
                                года
                                ТОО «Водные ресурсы-Маркетинг» уведомляет своих потребителей, что тарифы с 1 июля 2017
                                года по 1 июля 2018 года за 1м3
                                по оказанию услуг подачи воды по распределительным сетям и отводу сточных вод с вводом в
                                действие 01 июля 2017 года
                                установлены нижеследующие:</p>
                            <br>
                            <br>

                            <div class="text-uppercase report-title text-center">Услуги подачи воды по распределительным
                                сетям
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-report">
                                    <tr>
                                        <th>п/н</th>
                                        <th>Группа потребителей</th>
                                        <th>без НДС</th>
                                        <th>c НДС</th>
                                    </tr>
                                    <tr>
                                        <td>1 гр</td>
                                        <td>Населения и теплоснабжающие предприятия</td>
                                        <td>78,10</td>
                                        <td>87,47</td>
                                    </tr>
                                    <tr>
                                        <td>2 гр</td>
                                        <td>Государственные организации</td>
                                        <td>345,20</td>
                                        <td>386,62</td>
                                    </tr>
                                    <tr>
                                        <td>3 гр</td>
                                        <td>Прочие потребители не входящие в 1,2 группу</td>
                                        <td>267,25</td>
                                        <td>299,32</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="text-uppercase report-title text-center">Услуги по отводу сточных вод</div>
                            <div class="table-responsive">
                                <table class="table table-striped table-report">
                                    <tr>
                                        <th>п/н</th>
                                        <th>Группа потребителей</th>
                                        <th>без НДС</th>
                                        <th>c НДС</th>
                                    </tr>
                                    <tr>
                                        <td>1 гр</td>
                                        <td>Населения и теплоснабжающие предприятия</td>
                                        <td>78,10</td>
                                        <td>87,47</td>
                                    </tr>
                                    <tr>
                                        <td>2 гр</td>
                                        <td>Государственные организации</td>
                                        <td>345,20</td>
                                        <td>386,62</td>
                                    </tr>
                                    <tr>
                                        <td>3 гр</td>
                                        <td>Прочие потребители не входящие в 1,2 группу</td>
                                        <td>267,25</td>
                                        <td>299,32</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/structure.css">

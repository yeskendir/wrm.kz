<?php

/* @var $this yii\web\View */

$this->title = 'Archive';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Закупки</li>
        </ol>
        <h1 class="text-uppercase navigation-title">закупки</h1>
    </div>
</div>

<div class="info">
    <div class="container">
        <div class="row">
            <div class="info-left col-md-3 col-sm-12">
                <div class="even">
                    <div class="even-title">события <a href="#" class="even-yet-link">Ещё</a></div>
                    <div class="even-list">
                        <div class="even-item">
                            <a href="#" class="even-item-link">
                                <img src="/img/even-img-1.png" alt="Реконструкция водопроводной сети"
                                     class="img-responsive even-item-img">

                                <div class="even-item-anons">
                                    <div class="even-item-title">Реконструкция водопроводной сети</div>
                                    <div class="even-item-date">10.10.2017</div>
                                </div>
                            </a>
                        </div>

                        <div class="even-item">
                            <a href="#" class="even-item-link">
                                <img src="/img/even-img-2.png" alt="Об изменениях тарифов в г. Шымкенте"
                                     class="img-responsive even-item-img">

                                <div class="even-item-anons">
                                    <div class="even-item-title">Об изменениях тарифов в г. Шымкенте</div>
                                    <div class="even-item-date">10.10.2017</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-right col-md-9 col-sm-12">
                <div class="archive">
                    <div class="archive-title">Архив <a href="#" class="archive-yet-link">Ещё</a></div>

                    <div class="row">
                        <div class="bg-white archive-bg-white">
                            <div class="col-md-6">
                                <div class="archive-list">
                                    <div class="archive-item">
                                        <a href="#" class="archive-item-link">
                                            <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                                Шымкента 8 и 9 июня будет
                                                отключена вода
                                            </div>
                                            <div class="archive-item-date">04.09.2017</div>
                                        </a>
                                    </div>
                                    <div class="archive-item">
                                        <a href="#" class="archive-item-link">
                                            <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                                Шымкента 8 и 9 июня будет
                                                отключена вода
                                            </div>
                                            <div class="archive-item-date">04.09.2017</div>
                                        </a>
                                    </div>
                                    <div class="archive-item">
                                        <a href="#" class="archive-item-link">
                                            <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                                Шымкента 8 и 9 июня будет
                                                отключена вода
                                            </div>
                                            <div class="archive-item-date">04.09.2017</div>
                                        </a>
                                    </div>
                                    <div class="archive-item">
                                        <a href="#" class="archive-item-link">
                                            <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                                Шымкента 8 и 9 июня будет
                                                отключена вода
                                            </div>
                                            <div class="archive-item-date">04.09.2017</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="archive-list">
                                    <div class="archive-item">
                                        <a href="#" class="archive-item-link">
                                            <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                                Шымкента 8 и 9 июня будет
                                                отключена вода
                                            </div>
                                            <div class="archive-item-date">04.09.2017</div>
                                        </a>
                                    </div>
                                    <div class="archive-item">
                                        <a href="#" class="archive-item-link">
                                            <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                                Шымкента 8 и 9 июня будет
                                                отключена вода
                                            </div>
                                            <div class="archive-item-date">04.09.2017</div>
                                        </a>
                                    </div>
                                    <div class="archive-item">
                                        <a href="#" class="archive-item-link">
                                            <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                                Шымкента 8 и 9 июня будет
                                                отключена вода
                                            </div>
                                            <div class="archive-item-date">04.09.2017</div>
                                        </a>
                                    </div>
                                    <div class="archive-item">
                                        <a href="#" class="archive-item-link">
                                            <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                                Шымкента 8 и 9 июня будет
                                                отключена вода
                                            </div>
                                            <div class="archive-item-date">04.09.2017</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="/css/archive.css">

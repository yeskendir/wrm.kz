<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Личный кабинет</li>
        </ol>
        <h1 class="text-uppercase navigation-title">личный кабинет</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="info-menu">
                    <div class="info-menu-title text-uppercase">Литошенко Олег</div>
                    <div class="info-menu-list">
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Личные данные</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Печать квитанции</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Показания учета</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Оплатить за воду</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Выход</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <div class="text-uppercase report-title text-black">ОПЛАТА ЗА ВОДУ</div>
                            <div class="pay-kassa">Наличными через кассу</div>

                            <div class="table-responsive">
                                <table class="table table-striped table-report">
                                    <tr>
                                        <th>Место расположение</th>
                                        <th>Телефон</th>
                                        <th>Режим работы</th>
                                        <th>Выходные</th>
                                    </tr>
                                    <tr>
                                        <td>3 мкр дом б/н,(возле торгдома "Баянсулу")</td>
                                        <td>28-44-12</td>
                                        <td>9.00-19.00</td>
                                        <td>без выходных</td>
                                    </tr>
                                    <tr>
                                        <td>3 мкр дом б/н,(возле торгдома "Баянсулу")</td>
                                        <td>28-44-12</td>
                                        <td>9.00-19.00</td>
                                        <td>без выходных</td>
                                    </tr>
                                    <tr>
                                        <td>3 мкр дом б/н,(возле торгдома "Баянсулу")</td>
                                        <td>28-44-12</td>
                                        <td>9.00-19.00</td>
                                        <td>без выходных</td>
                                    </tr>
                                    <tr>
                                        <td>3 мкр дом б/н,(возле торгдома "Баянсулу")</td>
                                        <td>28-44-12</td>
                                        <td>9.00-19.00</td>
                                        <td>без выходных</td>
                                    </tr>
                                    <tr>
                                        <td>3 мкр дом б/н,(возле торгдома "Баянсулу")</td>
                                        <td>28-44-12</td>
                                        <td>9.00-19.00</td>
                                        <td>без выходных</td>
                                    </tr>
                                </table>
                            </div>
                            <p class="text-uppercase pay-attention">ВНИМАНИЕ! Обеденный перерыв с13.00 до14.00</p>

                            <div class="qiwi">
                                <div class="qiwi-title">Оплата через QIWI Терминалы</div>
                                <div class="qiwi-description">
                                    <img src="/img/qiwi.jpg" alt="" class="img-responsive qiwi-img">
                                    Оплатить наши услуги можно в любом QIWI Терминале по пути домой. QIWI Терминалы
                                    легко узнать по трем большим кнопкам на экране. Заплатить просто: достаточно нажать
                                    3-5 кнопок и внести наличные. Деньги будут зачислены моментально. Карта расположения
                                    терминалов и контактная информация
                                    <a href="#">на сайте</a>
                                </div>
                            </div>
                            <div class="bank">
                                <div class="bank-title">Оплата через банк</div>
                                <div class="bank-description">Вы можете оплатить услуги ТОО "Водные ресурсы Маркетинг"
                                    через любой из нижеперечисленных банков.
                                    Для оплаты при себе необходимо иметь квитанцию на оплату.
                                </div>
                                <div class="bank-list">
                                    <div class="bank-item">
                                        <img src="/img/bank-senim.jpg" alt="Сен1м банк" class="img-responsive">
                                    </div>
                                    <div class="bank-item">
                                        <img src="/img/bank-halyk.jpg" alt="halyk" class="img-responsive">
                                    </div>
                                    <div class="bank-item">
                                        <img src="/img/bank-sesna.jpg" alt="sesna" class="img-responsive">
                                    </div>
                                    <div class="bank-item">
                                        <img src="/img/bank-temir.jpg" alt="temir" class="img-responsive">
                                    </div>
                                    <div class="bank-item">
                                        <img src="/img/bank-euras.jpg" alt="euras" class="img-responsive">
                                    </div>
                                    <div class="bank-item">
                                        <img src="/img/bank-alians.jpg" alt="alinas" class="img-responsive">
                                    </div>
                                    <div class="bank-item">
                                        <img src="/img/bank-atf.jpg" alt="atf" class="img-responsive">
                                    </div>
                                    <div class="bank-item">
                                        <img src="/img/bank-bta.jpg" alt="bta" class="img-responsive">
                                    </div>
                                    <div class="bank-item">
                                        <img src="/img/bank-rbk.jpg" alt="rbk" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <div class="kazkom">
                                <div class="kazkom-title">Платежной картой через интернет</div>
                                <div class="kazkom-description">
                                    Вы можете оплатить наши услуги через специальный интернет сайты, используя платежную
                                    карту.
                                    На сайте Казкоммерцбанка <a href="#">перейти</a>
                                    Для осуществления платежа у Вас должна быть платежная карта Казкоммерцбанка.
                                    Чтобы начать, Вы должны зарегистрировать свою карту для осуществления платежа -
                                    регистрировать - перейти
                                    Дальнейшие инструкции Вы найдете на сайте Казкоммерцбанка.
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/cabinet-data.css">

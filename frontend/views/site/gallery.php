<?php

/* @var $this yii\web\View */

$this->title = 'Gallery';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Структура</li>
        </ol>
        <h1 class="text-uppercase navigation-title">СТРУКТУРА</h1>
    </div>
</div>


<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="info-menu">
                    <div class="info-menu-title text-uppercase">галерея</div>
                    <div class="info-menu-list">
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Фото</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Видео</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Музей</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">ЕБРР</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Конференции и семинары</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="gallery bg-white">
                        <div class="text-uppercase gallery-title text-black">Реконструкция водопроводной сети</div>
                        <div class="gallery-list">
                            <div class="gallery-item">
                                <img src="/img/gallery-1.jpg" alt="" class="img-responsive center-block">
                            </div>
                            <div class="gallery-item">
                                <img src="/img/gallery-2.jpg" alt="" class="img-responsive center-block">
                            </div>
                            <div class="gallery-item">
                                <img src="/img/gallery-3.jpg" alt="" class="img-responsive center-block">
                            </div>
                            <div class="gallery-item">
                                <img src="/img/gallery-1.jpg" alt="" class="img-responsive center-block">
                            </div>
                            <div class="gallery-item">
                                <img src="/img/gallery-2.jpg" alt="" class="img-responsive center-block">
                            </div>
                            <div class="gallery-item">
                                <img src="/img/gallery-3.jpg" alt="" class="img-responsive center-block">
                            </div>
                        </div>
                        <div class="text-uppercase gallery-title text-black">Реконструкция водопроводной сети</div>
                        <div class="gallery-list">
                            <div class="gallery-item">
                                <img src="/img/gallery-1.jpg" alt="" class="img-responsive center-block">
                            </div>
                            <div class="gallery-item">
                                <img src="/img/gallery-2.jpg" alt="" class="img-responsive center-block">
                            </div>
                            <div class="gallery-item">
                                <img src="/img/gallery-3.jpg" alt="" class="img-responsive center-block">
                            </div>
                            <div class="gallery-item">
                                <img src="/img/gallery-1.jpg" alt="" class="img-responsive center-block">
                            </div>
                            <div class="gallery-item">
                                <img src="/img/gallery-2.jpg" alt="" class="img-responsive center-block">
                            </div>
                            <div class="gallery-item">
                                <img src="/img/gallery-3.jpg" alt="" class="img-responsive center-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/gallery.css">

<?php

/* @var $this yii\web\View */

$this->title = 'Zakupki';
?>

<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Закупки</li>
        </ol>
        <h1 class="text-uppercase navigation-title">закупки</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="main-container">
            <div class="zakupki">
                <div class="zakupki-date bg-white">
                    <form class="form-inline form-date">
                        <div class="form-group">
                            <label for="inputDateStart">Дата с:</label>
                            <input type="date" class="form-control" id="inputDateStart">
                        </div>
                        <div class="form-group">
                            <label for="inputDateEnd">по:</label>
                            <input type="date" class="form-control" id="inputDateEnd">
                        </div>
                        <button type="submit" class="btn btn-info btn-date">Применить</button>
                        <div class="clearfix"></div>
                    </form>
                </div>

                <div class="zakupki-list">
                    <div class="zakupki-item bg-white zakupki-active">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="zakupki-info">
                                    <div class="zakupki-number">Номер тендера: 349636</div>
                                    <div class="zakupki-title text-uppercase">Ремонтно-восстановительные работы
                                    </div>
                                    <div class="zakupki-description">
                                        <p>Описание</p>

                                        <p>ТОО «Водные ресурсы -Маркетинг» г.Шымкент, ул. Г.Орманова 17.- объявляет
                                            открытый тендер</p>
                                        <br>

                                        <p>Выделенная сумма, тенге</p>

                                        <p>3 000 000,00 тг без НДС</p>

                                        <p>Размер авансового платежа %</p>

                                        <p>0%</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="zakupki-status">
                                    <div class="zakupki-status-text">
                                        ДО ОКОНЧАНИЯ<br>ПОДАЧИ ЗАЯВОК ОСТАЛОСЬ:
                                    </div>
                                    <div class="zakupki-status-time">5 дней 18 часов</div>
                                </div>
                                <div class="zakupki-download">
                                    <div class="text-center">
                                        <a href="#" class="zakupki-download-file"><span
                                                class="icon-file"></span><span class="zakupki-download-file-text">Скачать<br>
                                                конкурсную документацию.</span></a>

                                        <div class="clearfix"></div>
                                        <button class="btn btn-info btn-status">Оставить заявку</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="zakupki-item bg-white">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="zakupki-info">
                                    <div class="zakupki-number">Номер тендера: 349636</div>
                                    <div class="zakupki-title text-uppercase">Ремонтно-восстановительные работы
                                    </div>
                                    <div class="zakupki-description">
                                        <p>Описание</p>

                                        <p>ТОО «Водные ресурсы -Маркетинг» г.Шымкент, ул. Г.Орманова 17.- объявляет
                                            открытый тендер</p>
                                        <br>

                                        <p>Выделенная сумма, тенге</p>

                                        <p>3 000 000,00 тг без НДС</p>

                                        <p>Размер авансового платежа %</p>

                                        <p>0%</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="zakupki-status">
                                    <div class="zakupki-status-text">
                                        тендер завершен
                                    </div>
                                </div>
                                <div class="zakupki-download">
                                    <div class="text-center">
                                        <a href="#" class="zakupki-download-file"><span
                                                class="icon-file"></span><span class="zakupki-download-file-text">Скачать<br>
                                                конкурсную документацию.</span></a>

                                        <div class="clearfix"></div>
                                        <button class="btn btn-info btn-status">Результаты</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>


        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>
<link rel="stylesheet" href="/css/zakupki.css">

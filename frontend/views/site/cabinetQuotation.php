<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Личный кабинет</li>
        </ol>
        <h1 class="text-uppercase navigation-title">личный кабинет</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="info-menu">
                    <div class="info-menu-title text-uppercase">Литошенко Олег</div>
                    <div class="info-menu-list">
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Личные данные</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Печать квитанции</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Показания учета</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Оплатить за воду</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Выход</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <div class="text-uppercase report-title text-black">ПЕЧАТЬ КВИТАНЦИИ</div>

                            <div class="table-responsive">
                                <table class="table table-striped table-report">
                                    <tr>
                                        <th>Документ</th>
                                        <th>Скачать</th>
                                        <th>Размер</th>
                                    </tr>
                                    <tr>
                                        <td>Информация для абонента относительно квитанции</td>
                                        <td><a href="#"><span class="icon-pdf"></span></a></td>
                                        <td>51 KB</td>
                                    </tr>
                                </table>
                            </div>

                            <button class="btn btn-print">
                                <span class="btn-print-text text-uppercase">Распечатать квитанцию</span>
                                <span class="icon-print"></span>
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/cabinet-data.css">

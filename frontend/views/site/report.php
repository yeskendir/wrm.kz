<?php

/* @var $this yii\web\View */

$this->title = 'Report';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Корпоративное управление</li>
        </ol>
        <h1 class="text-uppercase navigation-title">Корпоративное управление</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="info-menu">
                    <div class="info-menu-title text-uppercase">отчеты</div>
                    <div class="info-menu-list">
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Корпоративное управление</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Тарифная смета</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Итоги внутреннего аудита</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <form class="form-inline form-date">
                                <div class="form-group">
                                    <label for="inputDateStart">Дата с:</label>
                                    <input type="date" class="form-control" id="inputDateStart">
                                </div>
                                <div class="form-group">
                                    <label for="inputDateEnd">по:</label>
                                    <input type="date" class="form-control" id="inputDateEnd">
                                </div>
                                <button type="submit" class="btn btn-info btn-date">Применить</button>
                                <div class="clearfix"></div>
                            </form>

                            <div class="table-responsible">
                                <table class="table table-striped table-report">
                                    <tr>
                                        <th>Документ</th>
                                        <th>Скачать</th>
                                        <th>Размер</th>
                                    </tr>
                                    <tr>
                                        <td>Бухгалтерский баланс</td>
                                        <td><a href="#"><span class="icon-pdf"></span></a></td>
                                        <td>1.47 MB</td>
                                    </tr>
                                    <tr>
                                        <td>Расшифровка к бухгалтерскому балансу по дебиторской и кредиторской
                                            задолженности
                                        </td>
                                        <td><a href="#"><span class="icon-pdf"></span></a></td>
                                        <td>2.34 MB</td>
                                    </tr>
                                    <tr>
                                        <td>Отчет о прибылях и убытках (Ф. 3)</td>
                                        <td><a href="#"><span class="icon-word"></span></a></td>
                                        <td>51 KB</td>
                                    </tr>
                                    <tr>
                                        <td>Отчет о прибылях и убытках (приложение 3)</td>
                                        <td><a href="#"><span class="icon-all-file"></span></a></td>
                                        <td>18 KB</td>
                                    </tr>
                                    <tr>
                                        <td>Отчет об изменениях в капитале</td>
                                        <td><a href="#"><span class="icon-all-file"></span></a></td>
                                        <td>51 KB</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/report.css">

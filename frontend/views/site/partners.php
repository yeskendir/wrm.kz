<?php

use \yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'S-Management';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Партнеры</li>
        </ol>
        <h1 class="text-uppercase navigation-title">ПАРТНЕРЫ</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('_navCompany', [
                    'title' => 'Партнеры'
                ]) ?>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <div class="text-uppercase report-title text-center">ПАРТНЕРЫ
                            </div>
                            <div class="report-body">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Взаимодействие с международными организациями
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                             aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <div class="h3 text-uppercase">ВЗАИМОДЕЙСТВИЕ С МЕЖДУНАРОДНЫМИ
                                                    ОРГАНИЗАЦИЯМИ
                                                </div>
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <a href="<?= Url::toRoute(['site/test']); ?>">Инвестиции
                                                            европейского банка реконструкции и
                                                            развития в обновление основных фондов ВКХ г. Шымкент</a>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <a href="<?= Url::toRoute(['site/test']); ?>">Делегация
                                                            Будапештского водоканала</a>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <a href="<?= Url::toRoute(['site/test']); ?>">Делегаты из Индии,
                                                            Японии, Турции и КНР</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingTwo">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse"
                                                   data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                                   aria-controls="collapseTwo">
                                                    Конференции и семинары
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingTwo">
                                            <div class="panel-body">
                                                <div class="h3 text-uppercase">Конференции и семинары</div>
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <a href="<?= Url::toRoute(['site/test']); ?>">Конференции и
                                                            семинары</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingThree">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse"
                                                   data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                                                   aria-controls="collapseThree">
                                                    Сотрудничество
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingThree">
                                            <div class="panel-body">
                                                <div class="h3 text-uppercase">Сотрудничество</div>
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <a href="<?= Url::toRoute(['site/test']); ?>">Делегация из
                                                            Турции</a>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <a href="<?= Url::toRoute(['site/test']); ?>">Народные
                                                            избранники встретились с южноказахстанцами</a>
                                                    </li>
                                                    <li class="list-group-item">
                                                        <a href="<?= Url::toRoute(['site/test']); ?>">Государственно-частное
                                                            партнерство</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/history.css">

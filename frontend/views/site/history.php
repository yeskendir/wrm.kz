<?php

/* @var $this yii\web\View */

$this->title = 'History';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">История</li>
        </ol>
        <h1 class="text-uppercase navigation-title">история</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('_navCompany', [
                    'title' => 'история'
                ]) ?>
            </div>
            <div class="col-md-9">
                <div class="main-container bg-white">
                    <div class="history">
                        <div class="history-anons">
                            Согласно приказу Горкомхоза № 103 от 28 ноября 1951 года в г. Чимкенте было организовано
                            управление "Водоканал". До времени организации "Водоканала" население города
                            пользовалось водой из родников, колодцев и речек. Водоснабжение в период с 1951 по 1957
                            годы состояло из 2-х насосных станций, расположенных в районе речки Кошкарата. Мощность
                            насосов доходила до 1255 м3 в сутки, протяженность сетей составляла 18,5 км. Вода,
                            поднимаемая двумя насосными станциями, продавалась по цене - 1 копейка за 2 ведра.
                        </div>
                        <img src="/img/history.jpg" class="img-responsive" alt="История">

                        <div class="history-description">
                            <ul class="history-list">
                                <li class="history-item"><span class="history-year">1952</span><span
                                            class="icon-dot"></span><span class="history-text"> построена первая линия канализации диаметром 200 мм, протяженностью 2 км по проспекту Коммунистический;</span>
                                </li>
                                <li class="history-item"><span class="history-year">1953</span><span
                                            class="icon-dot"></span><span class="history-text"> построен коллектор диаметром 600 мм, протяженностью 3 км. Введены в эксплуатацию первые очистные канализационные сооружения города мощностью 17 тысяч  м3/сут;</span>
                                </li>
                                <li class="history-item"><span class="history-year">1957</span><span
                                            class="icon-dot"></span><span class="history-text"> начато строительство первого водозабора в районе Бадам-Сайрамского створа, мощностью 0,8 м3/сек;</span>
                                </li>
                                <li class="history-item"><span class="history-year">1968</span><span
                                            class="icon-dot"></span><span class="history-text"> сдан в эксплуатацию Бадам-Сайрамский водозабор. Начато строительство станций второго подъема с двумя резервуарами;</span>
                                </li>
                                <li class="history-item"><span class="history-year">1972</span><span
                                            class="icon-dot"></span><span class="history-text"> построен водозабор Тассай 1 по проекту института "Казводоканалпроект";</span>
                                </li>
                                <li class="history-item"><span class="history-year">1973</span><span
                                            class="icon-dot"></span><span class="history-text"> введен в эксплуатацию водозабор Тассай 2;</span>
                                </li>
                                <li class="history-item"><span class="history-year">1983</span><span
                                            class="icon-dot"></span><span class="history-text"> сдан в эксплуатацию Акбай-Карасуйский водозабор из 32 скважин;</span>
                                </li>
                                <li class="history-item"><span class="history-year">1986</span><span
                                            class="icon-dot"></span><span class="history-text"> начато строительство блока механической очистки сточных вод городских очистных сооружений;</span>
                                </li>
                                <li class="history-item"><span class="history-year">1989</span><span
                                            class="icon-dot"></span><span class="history-text"> завершено строительство Кумыш-Булакского водозабора;</span>
                                </li>
                                <li class="history-item"><span class="history-year">1996</span><span
                                            class="icon-dot"></span><span class="history-text"> введен в эксплуатацию блок биологической очистки сточных вод;</span>
                                </li>
                                <li class="history-item"><span class="history-year">1998</span><span
                                            class="icon-dot"></span><span class="history-text"> начата реализация программы учета водопотребления с установкой приборов учета воды на распределительных сетях и у потребителей.</span>
                                </li>
                                <li class="history-item"><span class="history-year">1999</span><span
                                            class="icon-dot"></span><span class="history-text"> организовано ТОО "Водные ресурсы-Маркетинг";</span>
                                </li>
                                <li class="history-item"><span class="history-year">2001</span><span
                                            class="icon-dot"></span><span class="history-text"> усовершенствована система подачи воды за счет пьезометрической съемки перепадов давления;</span>
                                </li>
                                <li class="history-item"><span class="history-year">2006</span><span
                                            class="icon-dot"></span><span class="history-text"> начата реализация проекта реконструкции водопроводных сетей с выносом приборов учета воды в распределительные узлы.</span>
                                </li>
                                <li class="history-item"><span class="history-year">2008</span><span
                                            class="icon-dot"></span><span class="history-text"> получен сертификат соответствия международному стандарту ИСО 9001:2000.
                                        предприятие награждено:<br>
                                        "Золотым Сертификатом качества";<br>
                                        Знаком Почета "Лидер национальной экономики";<br>
                                        Знаком Почета "Лучшая компания СНГ";<br>
                                        Дипломом Министерства финансов РК "Лучший налогоплательщик по ЮКО".</span></li>
                                <li class="history-item"><span class="history-year">2009</span><span
                                            class="icon-dot"></span><span class="history-text">освоение инвестиции Европейского Банка Реконструкции и Развития для реализации Программы Приоритетного Инвестирования.</span>
                                </li>
                                <li class="history-item"><span class="history-year">2010</span><span
                                            class="icon-dot"></span><span class="history-text">Предприятие награждено дипломами "Золотой ягуар" за безупречную репутацию в бизнесе и высокое качество продукции и услуги и "Лавры славы" за профессиональные достижения в современном мире и вклад в интеллектуальное развитие общества по Международной имиджевой программе "Лидер ХХІ столетия" г. Москва.</span>
                                </li>
                                <li class="history-item"><span class="history-year">2011</span><span
                                            class="icon-dot"></span><span class="history-text">для реализации Проекта модернизации очистных сооружений и строительства установки биогаз подписан Кредитный договор с Европейским Банком Реконструкции и Развития на льготных условиях в сумме 13,6 млн. ЕВРО (2,8 млрд. тенге) на 2011-2013 г.г. Срок кредитования 12 лет.</span>
                                </li>
                                <li class="history-item"><span class="history-year">2012</span><span
                                            class="icon-dot"></span><span class="history-text">проведена модернизация оборудования очистных сооружений.
                                        начато строительство установки биогаза.<br>
                                        Общественной организацией "Региональная лига потребителей "Қыран" происвоен ТОО "Водные ресурсы-Маркетинг" народный знак качества "Алтын қран".<br>
                                        Организационным комитетом народного независимого конкурса "Лидер года" ТОО "Водные ресурсы-Маркетинг" награждено дипломом в номинации "Лучшее предприятие года" и присвоено почетное звание "Золотые лидеры Южного Казахстана".</span>
                                </li>
                                <li class="history-item"><span class="history-year">2013</span><span
                                            class="icon-dot"></span><span class="history-text">за существенный вклад в экологию ТОО "Водные ресурсы-Маркетинг" вручена государственная награда: знак и диплом серебряного призера республиканского конкурса по социальной ответственности бизнеса "Парыз-2012".<br>
                                        Сертификат качества «SIQS» Швейцарского института стандартов качества «International Quality certificate», подтверждающий, что услуги предприятия отличаются высоким качеством, конкурентоспособностью и могут быть рекомендованы международными экспертами к продвижению на мировой рынок.<br>
                                        Памятный диплом и атрибут<br>
                                        «UNITEDEUROPEAWARD»награждается предприятие, Орденским знаком и именным сертификатом «UNITEDEUROPEAWARD»награждается руководитель Европейской Бизнес Ассамблеи  в области маркетинга.</span>
                                </li>
                                <li class="history-item"><span class="history-year">2015</span><span
                                            class="icon-dot"></span><span class="history-text">компания ТОО "Водные ресурсы-Маркетинг" награждена  флагом  и сертификатом инвестиционной привлекательности "Flag of Europe" , а также отличительным знаком качества и лицензией "Prime Quality Standard"  Европейской Бизнес  Ассамблеей.
                                </li>
                                <span class="history-text">
                                    по результатам конкурса "Парыз"-2015 по социальной ответственности бизнеса Министром
                                    здравохранения и социального развития РК Дуйсеновой Т.К. ТОО "Водные
                                    ресурсы-Маркетинг" награждено Дипломом среди субъектов среднего и малого
                                    предпринимательства.<br>
                                    в Министерстве национальной экономики Республики Казахстан при участии Министра
                                    Досаева Е.Л. подписан кредитный договор между Европейским банком реконструкции и
                                    развития и ТОО «Водные ресурсы Маркетинг» на предоставление заемных средств на сумму
                                    10,0 млн.Евро в эквиваленте 3,1 млрд. тенге.<br></span>
                                <span class="clearfix"></span>
                                </li>

                            </ul>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/history.css">

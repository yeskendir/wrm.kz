<?php

/* @var $this yii\web\View */

$this->title = 'Contact';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">История</li>
        </ol>
        <h1 class="text-uppercase navigation-title">история</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="contact-map">
                    <div class="contact-map-title text-uppercase">схема проезда</div>
                    <div class="map">
                        <img src="/img/map.jpg" alt="Карта" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="main-container">
                    <div class="contact-info">
                        <div class="contact-info-title text-uppercase">контактные данные</div>
                        <div class="contact-info-container bg-white">
                            <div class="contact-info-company text-uppercase">ТОО «Водные ресурсы-Маркетинг»</div>
                            <div class="contact-info-description">
                                <p class="text-uppercase">Республика Казахстан, г. Шымкент,<br>
                                    Адрес: г.Шымкент, ул. Г.Орманова 17</p>

                                <div>Телефонный код г. Шымкент: 8(7252)</div>
                                <div>Приемная: 321-195, 321-194, 326-009</div>
                                <div>Центральная диспетчерская служба: 321-274, 323-574</div>
                                <div>Горячая линия: 321-275</div>
                                <div>Инспекция водных балансов населения: 321-180</div>
                                <div>Водомерная мастерская: 373-362, 375-457</div>
                                <div>Факс: 322-544</div>
                                <br>

                                <div class="contact-info-site text-uppercase">web: www.wrm.kz</div>
                                <div class="contact-info-email text-uppercase">E-mail: <a href="mailte:info@wrm.kz">info@wrm.kz</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="/css/contact.css">

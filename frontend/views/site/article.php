<?php

/* @var $this yii\web\View */

$this->title = 'Article';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li><a href="#" class="navigation-breadcrumb-link">Новости</a></li>
            <li class="active">Реконструкция водопроводной сети</li>
        </ol>
        <h1 class="text-uppercase navigation-title">Реконструкция водопроводной сети</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="main-container bg-white">
                    <div class="article">
                        <div class="article-title text-uppercase">Информация для абонентов</div>
                        <img src="/img/news-img.png" alt="" class="img-responsive article-img">

                        <div class="article-desc">
                            В 2017 году произведен реконструкция водопроводной сети с выносом ИВМ на 10 абонентов по
                            адресу Аль-Фарабийский район ул.Торекулова №75 дом. Хотим выразить свою благодарность
                            директору ТОО «Водные ресурсы-Маркетинг» Урманову М., за обеспечение народа чистой питьевой
                            водой. Огромное спасибо работникам ТОО «Ак булак суы» за вынос ИВМ по ул.Торекулова №75 дом.
                            Теперь показание счетчиков ежемесячно можно будет записывать не беспокоя абонентов.
                            Домком ул.Торекулова №75 дом. Тураев А.
                            <br>
                            <br>
                            В 2017 году произведен реконструкция водопроводной сети с выносом ИВМ на 10 абонентов по
                            адресу Аль-Фарабийский район ул.Торекулова №75 дом. Хотим выразить свою благодарность
                            директору ТОО «Водные ресурсы-Маркетинг» Урманову М., за обеспечение народа чистой питьевой
                            водой. Огромное спасибо работникам ТОО «Ак булак суы» за вынос ИВМ по ул.Торекулова №75 дом.
                            Теперь показание счетчиков ежемесячно можно будет записывать не беспокоя абонентов.
                            Домком ул.Торекулова №75 дом. Тураев А.
                            <br>
                            <br>
                            В 2017 году произведен реконструкция водопроводной сети с выносом ИВМ на 10 абонентов по
                            адресу Аль-Фарабийский район ул.Торекулова №75 дом. Хотим выразить свою благодарность
                            директору ТОО «Водные ресурсы-Маркетинг» Урманову М., за обеспечение народа чистой питьевой
                            водой. Огромное спасибо работникам ТОО «Ак булак суы» за вынос ИВМ по ул.Торекулова №75 дом.
                            Теперь показание счетчиков ежемесячно можно будет записывать не беспокоя абонентов.
                            Домком ул.Торекулова №75 дом. Тураев А.
                        </div>
                        <div class="share">
                            <span class="icon-share"></span>
                            <span class="share-name">ПОДЕЛИТЬСЯ</span>
                        </div>
                    </div>


                </div>
            </div>
            <div class="col-md-4">
                <div class="article-news">
                    <div class="article-news-list">
                        <div class="article-news-item article-news-vertical bg-white">
                            <div class="article-news-left">
                                <img src="/img/article-news-1.jpg"
                                     class="img-responsive article-news-left-img hidden-xs" alt="">
                                <img src="/img/article-news-2.jpg"
                                     class="img-responsive article-news-left-img hidden-lg hidden-md hidden-sm" alt="">
                            </div>
                            <div class="article-news-right">
                                <div class="article-news-right-title"><a href="#" class="article-news-link">Благодарственное
                                        письмо</a></div>
                                <div class="article-news-right-desc">Благодарим водоканал отдел канализации за хорошую
                                    работу за быструю ...
                                </div>
                                <div class="article-news-right-date">10.08.2017</div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="article-news-item article-news-horizontal bg-white">
                            <div class="article-news-top">
                                <img src="/img/article-news-2.jpg" alt="" class="img-responsive article-news-top-img">
                            </div>
                            <div class="article-news-bottom">
                                <div class="article-news-bottom-title">Квартком Даулеталы Мыктыбай</div>
                                <div class="article-news-bottom-desc">Работаю совместно с ТОО «Водные ресурсы-Маркетинг»
                                    со дня избрания меня председателем квартального ...
                                </div>
                                <div class="article-news-bottom-date">04.09.2017</div>
                            </div>
                        </div>
                        <div class="article-news-item article-news-vertical bg-white">
                            <div class="article-news-left">
                                <img src="/img/article-news-1.jpg"
                                     class="img-responsive article-news-left-img hidden-xs" alt="">
                                <img src="/img/article-news-2.jpg"
                                     class="img-responsive article-news-left-img hidden-lg hidden-md hidden-sm" alt="">
                            </div>
                            <div class="article-news-right">
                                <div class="article-news-right-title"><a href="#" class="article-news-link">Благодарственное
                                        письмо</a></div>
                                <div class="article-news-right-desc">Благодарим водоканал отдел канализации за хорошую
                                    работу за быструю ...
                                </div>
                                <div class="article-news-right-date">10.08.2017</div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="article-news-item article-news-horizontal bg-white">
                            <div class="article-news-top">
                                <img src="/img/article-news-2.jpg" alt="" class="img-responsive article-news-top-img">
                            </div>
                            <div class="article-news-bottom">
                                <div class="article-news-bottom-title">Квартком Даулеталы Мыктыбай</div>
                                <div class="article-news-bottom-desc">Работаю совместно с ТОО «Водные ресурсы-Маркетинг»
                                    со дня избрания меня председателем квартального ...
                                </div>
                                <div class="article-news-bottom-date">04.09.2017</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="/css/article.css">

<?php

/* @var $this yii\web\View */

$this->title = 'Quiz';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Опрос потребителей</li>
        </ol>
        <h1 class="text-uppercase navigation-title">Опрос потребителей</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="info-menu">
                    <div class="info-menu-title text-uppercase">потребителю</div>
                    <div class="info-menu-list">
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Вопросы-ответы</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Информация для абонентов</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Отчеты перед потребителем</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Тарифы</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Опрос потребителей</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Запрос задолженности</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="main-container bg-white">
                    <div class="quiz">
                        <div class="quiz-title text-uppercase text-center">
                            Опрос потребителей относительно стандартов качества услуг
                            ТОО «Водные ресурсы-Маркетинг»
                        </div>
                        <div class="quiz-desc">
                            <p>В настоящее время Комитетом по регулированию естественных монополий и защите
                                конкуренции Министерства
                                национальной экономики Республики Казахстан (далее - КРЕМЗК) в пилотном режиме
                                проводится разработка проектов
                                стандартов качества услуги по подаче воды по распределительным сетям.</p>

                            <p>В дальнейшем КРЕМЗК будет проводиться работа по разработке стандартов качества для
                                всех услуг субъектов
                                естественных монополий.</p>

                            <p> В этой связи, просим Вас принять участие в опросе по оценке качества услуги по
                                подаче воды по распределительным
                                сетям.</p>

                            <p>Цель опроса – оценка предпочтений потребителей по стандартам качества услуги и их
                                целевым показателям.</p>
                        </div>
                        <form class="form-quiz">
                            <div class="form-group form-type-user">
                                <label for="exampleInputEmail1">Кого Вы представляете:</label>

                                <div class="clearfix"></div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1"
                                               checked>
                                        Физическое лицо
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Юридическое лицо
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Укажите ваш ИИН/БИН: <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="exampleInputPassword1">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Номер телефона: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="exampleInputFile">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail">Адрес электроной почты: <span
                                        class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="exampleInputEmail">
                            </div>

                            <div class="form-group">
                                <p>В 2015 году у ТОО «Водные ресурсы - Маркетинг» зафиксировано 22 случая перерывов (в
                                    течение трех часов и более)
                                    в предоставлении услуги. В 2020 году ТОО «Водные ресурсы-Маркетинг» планирует
                                    снизить количество перерывов до 10.
                                    Укажите, пожалуйста, удовлетворены ли Вы таким снижением:</p>

                                <div class="clearfix"></div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1"
                                               checked>
                                        Абсолютно удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Скорее удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Скорее не удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Абсолютно не удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Не могу оценить
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <p>В 2015 году у ТОО «Водные ресурсы - Маркетинг» зафиксирован показатель аварийности,
                                    равный 2 аварий на 10 км
                                    трубопровода. ТОО «Водные ресрусы-Маркетинг» не планирует снижать в 2020 году
                                    аварийность.
                                    Укажите, пожалуйста, удовлетворены ли Вы тем, что количество аварий не снизится в
                                    2020 году, останется на уровне
                                    2 аварий на 10 км трубопровода:</p>

                                <div class="clearfix"></div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1"
                                               checked>
                                        Абсолютно удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Скорее удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Скорее не удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Абсолютно не удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Не могу оценить
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <p>В 2015 году в ТОО «Водные ресурсы - Маркетинг» 0,15% питьевой воды не соответствовало
                                    требованиям качества.
                                    В 2020 году ТОО «Водные ресурсы - Маркетинг» планирует снизить показатель
                                    некачественной питьевой воды до 0,06%.
                                    Удовлетворены ли Вы таким увеличением качества питьевой воды в 2020 году:</p>

                                <div class="clearfix"></div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1"
                                               checked>
                                        Абсолютно удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Скорее удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Скорее не удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Абсолютно не удовлетворен
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                        Не могу оценить
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info">Отправить</button>
                                <button type="submit" class="btn btn-primary">Результат</button>
                            </div>
                            <div class="quiz-result">Всего анкет: 21144</div>
                            <div class="quiz-result">Физические лица: 21132</div>
                            <div class="quiz-result">Юридические лица: 12</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/quiz.css">

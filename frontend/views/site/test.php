<?php

/* @var $this yii\web\View */

$this->title = 'S-Management';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Test</li>
        </ol>
        <h1 class="text-uppercase navigation-title">Test</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('_navCompany', [
                    'title' => 'Интегрированная система менеджмента'
                ]) ?>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <div class="text-uppercase report-title text-center">Test</div>
                            <div class="report-body">
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius enim hic laudantium
                                    non nostrum quidem voluptates. Aliquam dicta dolorem labore maxime quas unde, ut?
                                    Accusamus excepturi fugit illo minus nihil.
                                </div>
                                <div>Aliquam amet blanditiis cum cupiditate dolore dolorem eius eveniet exercitationem
                                    inventore ipsum magni, mollitia natus nostrum officia perferendis porro praesentium
                                    qui quisquam saepe sapiente suscipit vero vitae voluptas voluptatem voluptates!
                                </div>
                                <div>A aperiam architecto autem cum deleniti dolore dolores eaque enim error eveniet
                                    ipsum labore libero optio, placeat possimus praesentium quia recusandae saepe
                                    sapiente sed vel velit vero voluptatem. Quas, similique?
                                </div>
                                <div>Atque hic impedit iure laboriosam laudantium magni non officiis quidem quis,
                                    quisquam ullam vero vitae voluptatem? Animi corporis ducimus eaque harum ipsa
                                    molestias perferendis praesentium rerum sapiente sequi. Blanditiis, repellendus!
                                </div>
                                <div>Aliquid assumenda dolor doloremque in nemo nesciunt nihil odio quod. Accusamus
                                    adipisci amet debitis, deleniti dolorem doloremque earum excepturi fuga impedit
                                    inventore ipsa maiores nostrum omnis quibusdam quidem vel voluptas.
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor doloribus, ea esse
                                    eveniet ipsam labore minus nemo nostrum numquam pariatur perferendis perspiciatis
                                    quisquam reiciendis repellat sequi tempora temporibus vel velit?
                                </div>
                                <div>A aliquam cumque dignissimos doloribus eius enim eos est eveniet, facere facilis
                                    in, iure maiores, modi molestias nesciunt obcaecati odio omnis pariatur quibusdam
                                    quod sequi soluta suscipit ut! Aliquid, eligendi.
                                </div>
                                <div>Aliquid commodi cupiditate dolor enim, esse facilis fuga id iste nam nisi officiis
                                    praesentium quo repellendus reprehenderit repudiandae sapiente sint, ullam vitae.
                                    Amet culpa fuga id laudantium neque ut, vel!
                                </div>
                                <div>Doloribus esse maxime nobis quae. A atque consequatur cum cupiditate dignissimos
                                    distinctio fugit laborum laudantium libero, magnam necessitatibus omnis optio
                                    placeat quae quo recusandae rerum saepe sapiente vel veritatis! Minus.
                                </div>
                                <div>A aliquid architecto at cupiditate delectus dolorem et facere fugit harum hic ipsam
                                    magnam natus necessitatibus, nihil obcaecati reprehenderit vero! A aut cum enim
                                    maxime necessitatibus provident quam quod temporibus.
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, consequuntur earum
                                    fugit incidunt iusto neque quam reprehenderit repudiandae sequi suscipit temporibus
                                    ut voluptates voluptatum? Eos labore minima porro rerum veniam!
                                </div>
                                <div>Aspernatur commodi minus non qui quibusdam quisquam voluptas. Cumque expedita in
                                    maiores maxime sapiente. Accusantium atque consequuntur deleniti facere fuga illum
                                    ipsa, magnam nam, neque nobis quisquam, sed temporibus voluptatibus?
                                </div>
                                <div>Assumenda beatae commodi cumque debitis ducimus, expedita harum hic in mollitia
                                    natus nemo porro quisquam, rem, sed voluptate? Aut, fugiat hic maiores quia quos
                                    sint ullam vel veritatis voluptate voluptatum?
                                </div>
                                <div>Ab architecto aspernatur aut beatae blanditiis cupiditate dolorem doloribus earum
                                    enim eos facere fuga hic illum iste laboriosam libero officia officiis, possimus qui
                                    quisquam rem repellat repellendus reprehenderit tenetur ullam!
                                </div>
                                <div>Adipisci aliquid amet, animi aperiam asperiores aspernatur commodi culpa cumque ea
                                    eos esse eveniet fugit hic impedit iusto, molestiae numquam optio quibusdam, sed
                                    vitae! Accusantium culpa deleniti enim ex repellendus.
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquam amet
                                    cum, deserunt, esse fugit in ipsa iste itaque iusto modi nisi quae reiciendis
                                    reprehenderit, sed sit voluptates? Dolore, voluptatibus.
                                </div>
                                <div>Esse, eum id? A commodi consectetur id iure voluptates. Ad aliquam consectetur
                                    deleniti distinctio ducimus expedita facere fuga, fugit hic id illum, nesciunt
                                    perferendis quam quasi quibusdam quis tempora tempore.
                                </div>
                                <div>Ad consectetur cumque deleniti distinctio dolor eaque earum esse eum exercitationem
                                    fugiat id impedit inventore, itaque nobis odit officia praesentium, quasi
                                    repellendus reprehenderit, repudiandae rerum sapiente sunt temporibus unde
                                    voluptatum.
                                </div>
                                <div>Assumenda atque commodi cumque dolores dolorum earum est fugiat illum labore
                                    laborum magni maiores, minus nisi perspiciatis, provident quam quas quasi recusandae
                                    reprehenderit repudiandae soluta, temporibus ut? Ad, aliquid, quae.
                                </div>
                                <div>Fugit minima nemo suscipit? Itaque molestiae nisi possimus sapiente. Culpa cumque
                                    ducimus eum exercitationem iusto laborum minima nam nemo, quia repellendus sint
                                    temporibus totam velit. Aliquid laudantium molestiae officiis quas!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/history.css">

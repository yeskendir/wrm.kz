<?php

/* @var $this yii\web\View */

$this->title = 'Management';
?>

<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Руководство</li>
        </ol>
        <h1 class="text-uppercase navigation-title">Руководство</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('_navCompany', [
                    'title' => 'Руководство'
                ]) ?>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="management">
                        <div class="management-list">
                            <div class="management-item bg-white">
                                <div class="management-left">
                                    <img src="/img/management-user.jpg" alt="management" class="img-responsive">
                                </div>
                                <div class="management-right">
                                    <div class="management-item-name text-uppercase">УРМАНОВ Мурад Анарбекович</div>
                                    <div class="management-item-position text-uppercase text-black">ГЕНЕРАЛЬНый
                                        ДИРЕКТОР
                                    </div>
                                    <div class="management-item-anons">
                                        Прием граждан:<br>
                                        по предварительной записи у помощника генерального директора
                                    </div>
                                    <div class="management-item-call">
                                        <a class="management-item-phone text-black" href="tel:87252321194">
                                            <span class="icon-phone"></span>
                                            <span class="management-item-phone-text">8(7252) 321 194</span>
                                        </a>
                                        <a href="mailto:info@wrm.kz" class="management-item-email text-black">
                                            <span class="icon-email"></span>
                                            <span class="management-item-email-text">info@wrm.kz</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="management-item bg-white">
                                <div class="management-left">
                                    <img src="/img/management-user.jpg" alt="management" class="img-responsive">
                                </div>
                                <div class="management-right">
                                    <div class="management-item-name text-uppercase">УРМАНОВ Мурад Анарбекович</div>
                                    <div class="management-item-position text-uppercase text-black">ГЕНЕРАЛЬНый
                                        ДИРЕКТОР
                                    </div>
                                    <div class="management-item-anons">
                                        Прием граждан:<br>
                                        по предварительной записи у помощника генерального директора
                                    </div>
                                    <div class="management-item-call">
                                        <a class="management-item-phone text-black" href="tel:87252321194">
                                            <span class="icon-phone"></span>
                                            <span class="management-item-phone-text">8(7252) 321 194</span>
                                        </a>
                                        <a href="mailto:info@wrm.kz" class="management-item-email text-black">
                                            <span class="icon-email"></span>
                                            <span class="management-item-email-text">info@wrm.kz</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="management-item bg-white">
                                <div class="management-left">
                                    <img src="/img/management-user.jpg" alt="management" class="img-responsive">
                                </div>
                                <div class="management-right">
                                    <div class="management-item-name text-uppercase">УРМАНОВ Мурад Анарбекович</div>
                                    <div class="management-item-position text-uppercase text-black">ГЕНЕРАЛЬНый
                                        ДИРЕКТОР
                                    </div>
                                    <div class="management-item-anons">
                                        Прием граждан:<br>
                                        по предварительной записи у помощника генерального директора
                                    </div>
                                    <div class="management-item-call">
                                        <a class="management-item-phone text-black" href="tel:87252321194">
                                            <span class="icon-phone"></span>
                                            <span class="management-item-phone-text">8(7252) 321 194</span>
                                        </a>
                                        <a href="mailto:info@wrm.kz" class="management-item-email text-black">
                                            <span class="icon-email"></span>
                                            <span class="management-item-email-text">info@wrm.kz</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/management.css">

<?php

/* @var $this yii\web\View */

$this->title = 'Тews';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Видео</li>
        </ol>
        <h1 class="text-uppercase navigation-title">Видео</h1>
    </div>
</div>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/news.css">

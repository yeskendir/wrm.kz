<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Личный кабинет</li>
        </ol>
        <h1 class="text-uppercase navigation-title">личный кабинет</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="info-menu">
                    <div class="info-menu-title text-uppercase">Литошенко Олег</div>
                    <div class="info-menu-list">
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Личные данные</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Печать квитанции</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Показания учета</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Оплатить за воду</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Выход</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <div class="text-uppercase report-title text-black">Личные данные абонента</div>
                            <div class="text-uppercase report-user">Литошенко Олег</div>
                            <div class="report-number">Лиц.номер 20268</div>

                            <div class="table-responsive">
                                <table class="table table-striped table-report">
                                    <tr>
                                        <td>Дата регистрации</td>
                                        <td>25.05.2013 10:57.19</td>
                                    </tr>
                                    <tr>
                                        <td>E-mail</td>
                                        <td>lio@lio.kz</td>
                                    </tr>
                                    <tr>
                                        <td>Ф.И.О.</td>
                                        <td>Литошенко Олег</td>
                                    </tr>
                                    <tr>
                                        <td>Адрес</td>
                                        <td>Туркестанская, д.11, кв.6</td>
                                    </tr>
                                    <tr>
                                        <td>Баланс абонента</td>
                                        <td>0,00, по расчету от "январь 2016"</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="text-uppercase report-title text-black">История оплат</div>

                            <div class="table-responsive">
                                <table class="table table-striped table-report">
                                    <tr>
                                        <th>* Дата зачета оплаты</th>
                                        <th>Сумма</th>
                                    </tr>
                                    <tr>
                                        <td>19.10.2015</td>
                                        <td>1865,00</td>
                                    </tr>
                                    <tr>
                                        <td>19.10.2015</td>
                                        <td>1865,00</td>
                                    </tr>
                                    <tr>
                                        <td>19.10.2015</td>
                                        <td>1865,00</td>
                                    </tr>
                                    <tr>
                                        <td>19.10.2015</td>
                                        <td>1865,00</td>
                                    </tr>
                                    <tr>
                                        <td>19.10.2015</td>
                                        <td>1865,00</td>
                                    </tr>
                                    <tr>
                                        <td>19.10.2015</td>
                                        <td>1865,00</td>
                                    </tr>
                                </table>
                            </div>
                            <p>* дата фактической оплаты и дата зачета оплаты может иметь разницу в 1-2 рабочих дня.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/cabinet-data.css">

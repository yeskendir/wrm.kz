<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Личный кабинет</li>
        </ol>
        <h1 class="text-uppercase navigation-title">личный кабинет</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="info-menu">
                    <div class="info-menu-title text-uppercase">Литошенко Олег</div>
                    <div class="info-menu-list">
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Личные данные</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Печать квитанции</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Показания учета</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Оплатить за воду</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Выход</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <div class="text-uppercase report-title text-black">ПОКАЗАНИЯ УЧЕТА</div>
                            <div class="indications-text">Введите текущие показания приборов учета</div>
                            <form class="form-indication">
                                <div class="form-group">
                                    <label for="inputIndicationOne">счетчик №1, Зав. №96419197 <span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="inputIndicationOne">
                                </div>
                                <div class="form-group">
                                    <label for="inputIndicationTwo">счетчик №2, Зав. №96419221 <span
                                            class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="inputIndicationTwo">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info">Отправить</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>


<link rel="stylesheet" href="/css/cabinet-data.css">

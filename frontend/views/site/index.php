<?php

use \yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
    <div class="main-carousel">
        <div class="owl-main owl-carousel owl-theme">
            <div class="item">
                <a href="<?= Url::toRoute(['site/history']); ?>">
                    <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
                </a>
            </div>
            <div class="item">
                <a href="<?= Url::toRoute(['site/history']); ?>">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
                </a>
            </div>
            <div class="item">
                <a href="<?= Url::toRoute(['site/history']); ?>">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
                </a>
            </div>
            <div class="item">
                <a href="<?= Url::toRoute(['site/history']); ?>">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
                </a>
            </div>
            <div class="item">
                <a href="<?= Url::toRoute(['site/history']); ?>">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
                </a>
            </div>
            <div class="item">
                <a href="<?= Url::toRoute(['site/history']); ?>">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
                </a>
            </div>
            <div class="item">
                <a href="<?= Url::toRoute(['site/history']); ?>">
                    <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
                </a>
            </div>
        </div>
    </div>
    <div class="company">
        <h1 class="company-title text-center">ТОО «Водные ресурсы-Маркетинг»</h1>

        <div class="company-desc text-center text-uppercase">Чистая вода - источник жизни</div>
        <div class="company-logo"><img src="/img/company-logo.png" alt="ТОО «Водные ресурсы-Маркетинг»"
                                       class="img-responsive center-block"></div>
    </div>
    <div class="service">
        <div class="service-title text-center text-uppercase">Услуги и сервисы</div>
        <div class="container">
            <div class="owl-service owl-carousel owl-theme">
                <div class="item">
                    <img src="/img/banner-service-1.png" class="img-responsive">
                </div>
                <div class="item">
                    <img src="/img/banner-service-2.png" class="img-responsive">
                </div>
                <div class="item">
                    <a href="<?= Url::toRoute(['site/answer']); ?>">
                        <img src="/img/banner-service-3.png" class="img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="check">
        <img src="/img/check-img.png" alt="" class="check-img img-responsive">
        <div class="check-container">
            <div class="container">
                <div class="check-box">
                    <div class="check-title text-uppercase">Сверяйте показания онлайн</div>
                    <div class="check-description">На нашем сайте вы можете воспользоваться "Системой ввода показаний
                        через интернет".
                        Система разработана для самостоятельного ввода текущих показаний водосчетчиков абонентами
                        жилого сектора (квартиры, дома). Показания, введенные посредством СВПИ принимаются
                        к расчету наравне с показаниями, передаваемыми по телефону.
                    </div>
                    <div class="check-anons">Для того, чтобы начать пользоваться услугой
                        абонентам необходимо зарегистрироваться.
                    </div>
                    <div class="check-form text-center">
                        <button class="btn btn-info check-btn js-modal-signup">Зарегистрироваться</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= \frontend\widgets\Info::widget(); ?>
    <div class="data">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-12 data-left">
                    <div class="data-left-title text-uppercase text-black">Информация для абонентов</div>
                    <div class="data-left-box bg-white">
                        <div class="row">
                            <div class="col-sm-6">

                                <div class="data-par">
                                    <div class="data-par-title text-uppercase"><span
                                                class="icon-home"></span>обслуживание<span
                                                class="icon-arrow-right"></span></div>
                                    <div class="data-list">
                                        <div class="data-item"><a href="<?= Url::toRoute(['site/answer']); ?>">Вопросы-ответы</a>
                                        </div>
                                        <div class="data-item"><a href="#">Сообщение для абонентов</a></div>
                                        <div class="data-item"><a href="#">Обслуживание приборов учета воды</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="data-par">
                                    <div class="data-par-title text-uppercase"><span class="icon-document"></span>документация<span
                                                class="icon-arrow-right"></span></div>
                                    <div class="data-list">
                                        <div class="data-item"><a href="<?= Url::toRoute(['site/report']); ?>">Отчеты
                                                перед потребителем</a></div>
                                        <div class="data-item"><a href="#">Тарифы</a></div>
                                        <div class="data-item"><a href="#">Порядок исполнения обращений</a></div>
                                        <div class="data-item"><a href="#">Порядок подключения к сетям водопровода и
                                                канализации</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 data-right">
                    <div class="data-right-title  text-uppercase text-black">ГЕНЕРАЛЬНый ДИРЕКТОР ТОО «Водные
                        ресурсы-маркетинг»
                    </div>
                    <a href="<?= Url::toRoute(['site/management']); ?>">
                    <img src="/img/boss.png" alt="УРМАНОВ Мурад Анарбекович"
                         class="data-right-img img-responsive center-block">
                    </a>
                    <div class="data-boss text-uppercase text-center">УРМАНОВ Мурад Анарбекович</div>
                    <div class="data-right-buttons text-center">
                        <div class="btn btn-info">Задать вопрос</div>
                        <div class="btn btn-primary">Опрос потребителей</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?= \frontend\widgets\CallBack::widget(); ?>

<?
$this->registerJsFile('@web/js/owl/owl.carousel.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile('@web/js/owl/assets/owl.carousel.min.css');
$this->registerCssFile('@web/js/owl/assets/owl.theme.default.min.css');
$js = <<<JS
jQuery(document).ready(function ($) {
        $('.owl-main').owlCarousel({
            items: 1,
            animateOut: 'fadeOut',
            loop: true,
            margin: 10
        });
        $('.owl-service').owlCarousel({
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    margin: 20
                },
                480: {
                    items: 2,
                    margin: 20
                },
                600: {
                    items: 3,
                    margin: 20
                },
                1000: {
                    items: 3,
                    margin: 200
                }
            },
            dots: false,
            nav: true,
            navText: ["<img src='/img/icon-caret-left.png' class='img-responsive'>", "<img src='/img/icon-caret-right.png' class='img-responsive'>"],
            loop: true,
        });
    });
JS;

$this->registerJs($js, \yii\web\View::POS_READY);

?>
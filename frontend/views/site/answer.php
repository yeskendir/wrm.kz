<?php

/* @var $this yii\web\View */

$this->title = 'Answer';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Вопросы-Ответы</li>
        </ol>
        <h1 class="text-uppercase navigation-title">ВОПРОСЫ-ОТВЕТЫ</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="info-menu">
                    <div class="info-menu-title text-uppercase">потребителю</div>
                    <div class="info-menu-list">
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Вопросы-ответы</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Информация для абонентов</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Отчеты перед потребителем</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Тарифы</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Опрос потребителей</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Запрос задолженности</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="main-container bg-white">
                    <div class="answer">
                        <div class="answer-title">Задайте вопрос об услугах водоснабжения и канализации в
                            электронном виде
                            и наши специалисты ответят на него в ближайшее время
                        </div>
                        <div class="answer-ask text-white"><span class="icon-question"></span> Задать вопрос</div>
                        <div class="answer-deadline">Срок предоставления<br> ответа - До 3 дней</div>
                        <div class="clearfix"></div>
                        <div class="answer-conditions">
                            <div class="conditions-question text-center">
                                <div class="conditions-question-number">15</div>
                                <div class="conditions-question-title">вопросов</div>
                            </div>
                            <div class="answer-conditions-anons">
                                <div class="answer-conditions-title">Технические условия</div>
                                <div class="answer-conditions-text">Вопросы получения Технических условий на
                                    подключение к городским сетям водопровода и канализации; на разработку
                                    проектно-сметной документации
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="answer-list">
                            <div class="media answer-item">
                                <div class="media-body">
                                    <h4 class="media-heading">Вопрос</h4>

                                    <div class="media-info">Опубликовано 03 Января 2018 в 21:36, сообщение
                                        <span class="media-info-number">№19318285</span>
                                    </div>
                                    <div class="media-name">Мырзакулова Бактыгуль Турсынбаевна</div>
                                    <div class="media-question">
                                        Моя недвижимая имущества зарегистрирована как хоз.постройки"Сарай"по
                                        вышеуказанному адресу,т.е. рядом между общежитиями
                                        бывшей Текстильной фабрики (Меланж) в данное время необходимо провести воду
                                        от общежитие ул.Терешкова,дом №18, примерно
                                        15 метров от наши здание.Какие нужны документы, в течение какого срока можно
                                        получит техническое условия.
                                    </div>

                                    <div class="media">
                                        <div class="media-left"></div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Ответ</h4>

                                            <div class="media-info">Опубликовано 03 Января 2018 в 21:36, сообщение
                                                <span class="media-info-number">№19318285</span>
                                            </div>
                                            <div class="media-name">Мырзакулова Б. Т.</div>
                                            <div class="media-question">
                                                ТОО "Водные ресурсы-Маркетинг" рассмотрев Ваше обращение от 31
                                                октября 2017 года за №69-M-W о выдаче технических
                                                условий на объект ул. Терешкова, д. 18А, сообщает о том, в пункте
                                                52. Приказа Министра национальной экономики
                                                Республики Казахстан от 30 ноября 2015 года № 750 "Об утверждении
                                                Правил организации застройки и прохождения
                                                разрешительных процедур в сфере строительства".
                                                <br>
                                                В связи с изложенным для получения технических условий, заявителю
                                                необходимо представить достоверные документы,
                                                согласно требованию "Приказа Министра национальной экономики
                                                Республики Казахстан от 29 декабря 2015 года №175.
                                                Об утверждении Правил предоставления равных условий доступа к
                                                регулируемым услугам (товарам, работам) в сфере
                                                естественных монополий".
                                                <br><br><br>
                                                исп. Д. А. Лесов
                                                <br>
                                                тел. 32-12-47 (222)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="share">
                                <span class="icon-share"></span>
                                <span class="share-name">ПОДЕЛИТЬСЯ</span>
                            </div>
                            <div class="answer-hr"></div>
                        </div>

                        <div class="answer-list">
                            <div class="media answer-item">
                                <div class="media-body">
                                    <h4 class="media-heading">Вопрос</h4>

                                    <div class="media-info">Опубликовано 03 Января 2018 в 21:36, сообщение
                                        <span class="media-info-number">№19318285</span>
                                    </div>
                                    <div class="media-name">Мырзакулова Бактыгуль Турсынбаевна</div>
                                    <div class="media-question">
                                        Моя недвижимая имущества зарегистрирована как хоз.постройки"Сарай"по
                                        вышеуказанному адресу,т.е. рядом между общежитиями
                                        бывшей Текстильной фабрики (Меланж) в данное время необходимо провести воду
                                        от общежитие ул.Терешкова,дом №18, примерно
                                        15 метров от наши здание.Какие нужны документы, в течение какого срока можно
                                        получит техническое условия.
                                    </div>

                                    <div class="media">
                                        <div class="media-left"></div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Ответ</h4>

                                            <div class="media-info">Опубликовано 03 Января 2018 в 21:36, сообщение
                                                <span class="media-info-number">№19318285</span>
                                            </div>
                                            <div class="media-name">Мырзакулова Б. Т.</div>
                                            <div class="media-question">
                                                ТОО "Водные ресурсы-Маркетинг" рассмотрев Ваше обращение от 31
                                                октября 2017 года за №69-M-W о выдаче технических
                                                условий на объект ул. Терешкова, д. 18А, сообщает о том, в пункте
                                                52. Приказа Министра национальной экономики
                                                Республики Казахстан от 30 ноября 2015 года № 750 "Об утверждении
                                                Правил организации застройки и прохождения
                                                разрешительных процедур в сфере строительства".
                                                <br>
                                                В связи с изложенным для получения технических условий, заявителю
                                                необходимо представить достоверные документы,
                                                согласно требованию "Приказа Министра национальной экономики
                                                Республики Казахстан от 29 декабря 2015 года №175.
                                                Об утверждении Правил предоставления равных условий доступа к
                                                регулируемым услугам (товарам, работам) в сфере
                                                естественных монополий".
                                                <br><br><br>
                                                исп. Д. А. Лесов
                                                <br>
                                                тел. 32-12-47 (222)
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="share">
                                <span class="icon-share"></span>
                                <span class="share-name">ПОДЕЛИТЬСЯ</span>
                            </div>
                            <div class="answer-hr"></div>
                        </div>

                        <div class="text-center panel-yet">
                            <button class="btn btn-info btn-yet"><span class="icon-plus"></span>Показать еще
                            </button>
                        </div>

                        <div class="answer-conditions answer-gray">
                            <div class="conditions-question text-center">
                                <div class="conditions-question-number">165</div>
                                <div class="conditions-question-title">вопросов</div>
                            </div>
                            <div class="answer-conditions-anons">
                                <div class="answer-conditions-title">Расчеты за услуги водоснабжения и
                                    водоотведения
                                </div>
                                <div class="answer-conditions-text">Вопросы начисления и оплаты за услуги
                                    водоснабжения и канализации
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="answer-conditions answer-gray">
                            <div class="conditions-question text-center">
                                <div class="conditions-question-number">42</div>
                                <div class="conditions-question-title">вопросов</div>
                            </div>
                            <div class="answer-conditions-anons">
                                <div class="answer-conditions-title">Обслуживание (эксплуатация) сетей водопровода и
                                    канализации
                                </div>
                                <div class="answer-conditions-text">
                                    <div>Вопросы по качеству питьевой воды;</div>
                                    <div>Вопросы по случаям отсутствия воды,снижения напора (давления) питьевой
                                        воды;
                                    </div>
                                    <div>Вопросы утечек, неисправностей, аварий на сетях водопровода и
                                        канализации.
                                    </div>
                                    <div>Вопросы засорения сетей канализации.</div>
                                    <div>Вопросы технического обслуживания внутридомовых сетей водопровода и
                                        канализации.
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="answer-conditions answer-gray">
                            <div class="conditions-question text-center">
                                <div class="conditions-question-number">82</div>
                                <div class="conditions-question-title">вопросов</div>
                            </div>
                            <div class="answer-conditions-anons">
                                <div class="answer-conditions-title">Приборы учета воды</div>
                                <div class="answer-conditions-text">Вопросы по установки, поверки, ремонта и замены
                                    водомеров
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="answer-conditions answer-gray">
                            <div class="conditions-question text-center">
                                <div class="conditions-question-number">23</div>
                                <div class="conditions-question-title">вопросов</div>
                            </div>
                            <div class="answer-conditions-anons">
                                <div class="answer-conditions-title">Реконструкция и замена сетей водопровода</div>
                                <div class="answer-conditions-text">Вопросы по реконструкции, замене изношенных
                                    водопроводных сетей (договора, расчеты, качество выполненных работ и др.)
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="answer-conditions answer-gray">
                            <div class="conditions-question text-center">
                                <div class="conditions-question-number">57</div>
                                <div class="conditions-question-title">вопросов</div>
                            </div>
                            <div class="answer-conditions-anons">
                                <div class="answer-conditions-title">Другое</div>
                                <div class="answer-conditions-text">Вопросы, касающиеся деятельности ТОО «Водные
                                    ресурсы-Маркетинг» по предоставлению услуг водоснабжения и канализации
                                    Предложения, замечания, отзывы.
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="call">
    <div class="container text-white">
        <div class="call-title text-uppercase text-center">обратная связь</div>
        <div class="row">
            <form>
                <div class="col-sm-6 call-left">
                    <div class="form-group">
                        <label for="callInputName" class="text-uppercase">Ф.И.О.:<span
                                class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="callInputName" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="callInputPhone">Телефон:<span class="text-danger">*</span></label>
                        <input type="tel" class="form-control" id="callInputPhone">
                    </div>
                    <div class="form-group">
                        <label for="callInputEmail">Email:</label>
                        <input type="email" class="form-control" id="callInputEmail">
                    </div>
                </div>
                <div class="col-sm-6 call-right">
                    <div class="form-group">
                        <label for="callInputComment">Комментарий:</label>
                        <textarea id="callInputComment" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="col-xs-12 text-center">
                    <button type="submit" class="btn btn-info btn-call-back">Отправить</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div class="info">
    <div class="container">
        <div class="row">
            <div class="info-left col-md-3 col-sm-12">
                <div class="even">
                    <div class="even-title">события <a href="#" class="even-yet-link">Ещё</a></div>
                    <div class="even-list">
                        <div class="even-item">
                            <a href="#" class="even-item-link">
                                <img src="/img/even-img-1.png" alt="Реконструкция водопроводной сети"
                                     class="img-responsive even-item-img">

                                <div class="even-item-anons">
                                    <div class="even-item-title">Реконструкция водопроводной сети</div>
                                    <div class="even-item-date">10.10.2017</div>
                                </div>
                            </a>
                        </div>

                        <div class="even-item">
                            <a href="#" class="even-item-link">
                                <img src="/img/even-img-2.png" alt="Об изменениях тарифов в г. Шымкенте"
                                     class="img-responsive even-item-img">

                                <div class="even-item-anons">
                                    <div class="even-item-title">Об изменениях тарифов в г. Шымкенте</div>
                                    <div class="even-item-date">10.10.2017</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-center col-md-6 col-sm-12">
                <div class="news">
                    <div class="news-title">новости <a href="#" class="news-yet-link">Ещё</a></div>

                    <div class="news-list">
                        <div class="row">
                            <div class="col-md-12 news-top">
                                <div class="news-item news-item-top">
                                    <a href="#" class="news-item-link">
                                        <div class="news-item-top-left">
                                            <img src="/img/news-img-1.png" alt="Реконструкция водопроводной сети"
                                                 class="img-responsive news-item-img">
                                        </div>
                                        <div class="news-item-top-right">
                                            <div class="news-item-top-anons">
                                                <div class="news-item-top-title">Капитальный ремонт</div>
                                                <div class="news-item-top-text">В октябре 2017 года произведен
                                                    капитальный
                                                    ремонт водопроводной сети диаметром 80 мм
                                                    в доме 42 в мкр.Терискей...
                                                </div>
                                                <div class="news-item-top-date">10.10.2017</div>
                                            </div>
                                        </div>
                                        <span class="clearfix"></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 news-left">
                                <div class="news-item news-item-left">
                                    <a href="#" class="news-item-link">
                                        <img src="/img/news-img-2.png" alt="Реконструкция водопроводной сети"
                                             class="img-responsive news-item-left-img hidden-sm hidden-xs">

                                        <img src="/img/news-img-1.png" alt="Реконструкция водопроводной сети"
                                             class="img-responsive news-item-left-img news-item-left-img-mob hidden-lg hidden-md">

                                        <div class="news-item-left-anons">
                                            <div class="news-item-left-title">Квартком Даулеталы Мыктыбай</div>
                                            <div class="news-item-left-text">Работаю совместно с ТОО «Водные
                                                ресурсы-Маркетинг» со дня избрания меня председателем квартального
                                                ...
                                            </div>
                                            <div class="news-item-left-date">04.09.2017</div>
                                        </div>
                                        <span class="clearfix"></span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6 news-right">
                                <div class="news-item news-item-right">
                                    <a href="#" class="news-item-link">
                                        <div class="news-item-right-top">
                                            <img src="/img/news-img-3.png"
                                                 alt="Реконструкция водопроводной сети"
                                                 class="img-responsive news-item-right-img hidden-sm hidden-xs">

                                            <img src="/img/news-img-1.png"
                                                 alt="Реконструкция водопроводной сети"
                                                 class="img-responsive news-item-right-img  news-item-right-img-mob  hidden-lg hidden-md">
                                        </div>
                                        <div class="news-item-right-bottom">
                                            <div class="news-item-right-anons">
                                                <div class="news-item-right-title">Благодарственное письмо</div>
                                                <div class="news-item-right-text">Благодарим водоканал отдел
                                                    канализации
                                                    за хорошую работу за быструю ...
                                                </div>
                                                <div class="news-item-right-date">10.08.2017</div>
                                            </div>
                                        </div>
                                        <span class="clearfix"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="info-right col-md-3 col-sm-12">
                <div class="archive">
                    <div class="archive-title">Архив <a href="#" class="archive-yet-link">Ещё</a></div>

                    <div class="archive-list">
                        <div class="archive-item">
                            <a href="#" class="archive-item-link">
                                <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                    Шымкента 8 и 9 июня будет
                                    отключена вода
                                </div>
                                <div class="archive-item-date">04.09.2017</div>
                            </a>
                        </div>
                        <div class="archive-item">
                            <a href="#" class="archive-item-link">
                                <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                    Шымкента 8 и 9 июня будет
                                    отключена вода
                                </div>
                                <div class="archive-item-date">04.09.2017</div>
                            </a>
                        </div>
                        <div class="archive-item">
                            <a href="#" class="archive-item-link">
                                <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                    Шымкента 8 и 9 июня будет
                                    отключена вода
                                </div>
                                <div class="archive-item-date">04.09.2017</div>
                            </a>
                        </div>
                        <div class="archive-item">
                            <a href="#" class="archive-item-link">
                                <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                    Шымкента 8 и 9 июня будет
                                    отключена вода
                                </div>
                                <div class="archive-item-date">04.09.2017</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="/css/answer.css">

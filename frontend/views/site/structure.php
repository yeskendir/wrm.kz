<?php

/* @var $this yii\web\View */

$this->title = 'Structure';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Структура</li>
        </ol>
        <h1 class="text-uppercase navigation-title">СТРУКТУРА</h1>
    </div>
</div>


<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('_navCompany', [
                    'title' => 'Структура'
                ]) ?>
            </div>


            <div class="col-md-9">
                <div class="main-container">
                    <div class="report">
                        <div class="report-date bg-white">
                            <div class="text-uppercase report-title text-center">ЭКСПЛУАТАЦИОННЫЕ УЧАСТКИ</div>
                            <div class="table-responsive">
                                <table class="table table-striped table-report">
                                    <tr>
                                        <th>№</th>
                                        <th>Подразделения</th>
                                        <th>Адрес</th>
                                        <th>Телефон</th>
                                        <th>e-mail</th>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Энбекшинский район</td>
                                        <td>ул. Валиханова №188 "А"</td>
                                        <td>57-66-65</td>
                                        <td><a href="mailto:rvk-1@wrm.kz">rvk-1@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Аль-Фарабийский район</td>
                                        <td>ул.Курнакова</td>
                                        <td>53-09-56</td>
                                        <td><a href="mailto:rvk-2@wrm.kz">rvk-2@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Абайский район</td>
                                        <td>4 мкр Фабрицоса бн н.п тойхана Асем ай</td>
                                        <td>46-10-38</td>
                                        <td><a href="mailto:rvk-3@wrm.kz">rvk-3@wrm.kz</a></td>
                                    </tr>

                                    <tr>
                                        <td>4</td>
                                        <td>Каратауский РВК</td>
                                        <td>мкр "Север" б/н</td>
                                        <td>-</td>
                                        <td><a href="mailto:rvk-4@wrm.kz">rvk-4@wrm.kz</a></td>
                                    </tr>

                                    <tr>
                                        <td>5</td>
                                        <td>Частный сектор №8</td>
                                        <td>ул Калдаякова №9</td>
                                        <td>56-24-92</td>
                                        <td><a href="mailto:chs-vik@wrm.kz">chs-vik@wrm.kz</a></td>
                                    </tr>

                                    <tr>
                                        <td>6</td>
                                        <td>Частный сектор №2</td>
                                        <td>ул.Байдибек би уг.ул.Алматинская трасса б/н</td>
                                        <td>47-61-82</td>
                                        <td><a href="mailto:chs-kaitpas@wrm.kz">chs-kaitpas@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Центральная диспетчерская служба</td>
                                        <td>ул.Г.Орманова №17</td>
                                        <td>32-12-74, <br>32-35-74, <br>32-12-71</td>
                                        <td><a href="mailto:cds@wrm.kz">cds@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>Участок по обслуживанию водомеров вынесенных на лестничные площадки</td>
                                        <td>ул. Валиханова №188 "А"</td>
                                        <td>57-66-67</td>
                                        <td><a href="mailto:vs-6@wrm.kz">vs-6@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>Частный сектор №1</td>
                                        <td>ул.Дулати №203 "а" БЦ "Табыс"</td>
                                        <td>32-11-53</td>
                                        <td><a href="mailto:chs-12@wrm.kz">chs-12@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>Частный сектор №3</td>
                                        <td>Ленгерский шоссе б/н</td>
                                        <td>43-05-63</td>
                                        <td><a href="mailto:chs-3@wrm.kz">chs-3@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>Частный сектор №9</td>
                                        <td>мкр. Казыгурт ул. Жандарбекова б/н</td>
                                        <td>50-52-42</td>
                                        <td><a href="mailto:chs-kazgurt@wrm.kz">chs-kazgurt@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>12</td>
                                        <td>Частный сектор №5</td>
                                        <td>ул.Галицына б/н</td>
                                        <td>53-38-04</td>
                                        <td><a href="mailto:chs-nturlan@wrm.kz">chs-nturlan@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>13</td>
                                        <td>Управление канализационными сетями</td>
                                        <td>ул.Г.Орманова №17</td>
                                        <td>35-35-98</td>
                                        <td><a href="mailto:innovation-7@wrm.kz">innovation-7@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>14</td>
                                        <td>Очистные сооружения</td>
                                        <td>ул.Г.Орманова №17</td>
                                        <td>27-07-65</td>
                                        <td><a href="mailto:biomeh@wrm.kz">biomeh@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>15</td>
                                        <td>Водомерная мастерская</td>
                                        <td>мкр "Север" б/н</td>
                                        <td>37-33-62, <br>37-54-57</td>
                                        <td><a href="mailto:vodomer@wrm.kz">vodomer@wrm.kz</a></td>
                                    </tr>

                                    <tr>
                                        <td>16</td>
                                        <td>Высотный сектор 1</td>
                                        <td>3 мкр.дом б/н (возле торг дома "Баян-сулу"</td>
                                        <td>28-44-12</td>
                                        <td><a href="mailto:vs-1@wrm.kz">vs-1@wrm.kz</a></td>
                                    </tr>

                                    <tr>
                                        <td>17</td>
                                        <td>Частный сектор №4</td>
                                        <td>ул. Валиханова №188 "А"</td>
                                        <td>57-66-67</td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>18</td>
                                        <td>Частный сектор №6</td>
                                        <td>ул.Дулати №203 "а" БЦ "Табыс"</td>
                                        <td>32-63-68</td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>19</td>
                                        <td>Частный сектор №7</td>
                                        <td>ул.Галицына б/н</td>
                                        <td>53-38-04</td>
                                        <td><a href="mailto:chs-nturlan@wrm.kz">chs-nturlan@wrm.kz</a></td>
                                    </tr>
                                </table>
                            </div>

                            <div class="text-uppercase report-title text-center">ДИЛЕРСКИЕ УЧАСТКИ</div>
                            <div class="table-responsive">
                                <table class="table table-striped table-report">
                                    <tr>
                                        <th>№</th>
                                        <th>Подразделения</th>
                                        <th>Адрес</th>
                                        <th>Телефон</th>
                                        <th>e-mail</th>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Свинцовый поселок уч. ВиК</td>
                                        <td>ул Калдаякова №9</td>
                                        <td>56-24-92</td>
                                        <td><a href="mailto:chs-vik@wrm.kz">chs-vik@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Частный сектор 3</td>
                                        <td>Капал батыр б/н</td>
                                        <td>43-05-63</td>
                                        <td><a href="mailto:chs-3@wrm.kz">chs-3@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Казыгурт</td>
                                        <td>мкр.Казыгурт, ул. Жандарбекова б/н</td>
                                        <td>50-52-42</td>
                                        <td><a href="mailto:chs-kazgurt@wrm.kz">chs-kazgurt@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td> Кайтпас. ГКП ШУ Водоканал</td>
                                        <td>уг. ул. Байдибек би, Алматинская трасса</td>
                                        <td>47-61-82</td>
                                        <td><a href="mailto:chs-kaitpas@wrm.kz">chs-kaitpas@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Участок по обслуживанию водомеров вынесенных на лестничные площадки,
                                            Частный сектор 4
                                        </td>
                                        <td>ул. Уалиханова №188 А</td>
                                        <td>57-66-67</td>
                                        <td><a href="mailto:vs-6@wrm.kz">vs-6@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Тельман</td>
                                        <td>уг. ул. Байдибек би, Алматинская трасса</td>
                                        <td>47-61-82</td>
                                        <td><a href="mailto:chs-kaitpas@wrm.kz">chs-kaitpas@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Высотный сектор 1</td>
                                        <td>3 мкр дом б/н (возле торг дома «Баянсулу»)</td>
                                        <td>28-44-12</td>
                                        <td><a href="mailto:vs-1@wrm.kz">vs-1@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>БАМ</td>
                                        <td>16 мкр б/н (авт/ост «Север»)</td>
                                        <td>49-15-82</td>
                                        <td><a href="mailto:vs-bam@wrm.kz">vs-bam@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>Частный сектор №7</td>
                                        <td>ул.Галицына б/н</td>
                                        <td>53-38-04</td>
                                        <td><a href="mailto:chs-nturlan@wrm.kz">chs-nturlan@wrm.kz</a></td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>уч. Частный сектор 1,2 (Су Арнасы 12В)</td>
                                        <td>ул.Дулати, №203 "А" БЦ "Табыс"</td>
                                        <td>32-11-53</td>
                                        <td><a href="mailto:chs-12@wrm.kz">chs-12@wrm.kz</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/structure.css">

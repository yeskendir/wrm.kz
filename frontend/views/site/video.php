<?php

/* @var $this yii\web\View */

$this->title = 'Video';
?>
<div class="navigation">
    <div class="container">
        <ol class="breadcrumb navigation-breadcrumb">
            <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
            <li class="active">Видео</li>
        </ol>
        <h1 class="text-uppercase navigation-title">Видео</h1>
    </div>
</div>

<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="info-menu">
                    <div class="info-menu-title text-uppercase">галерея</div>
                    <div class="info-menu-list">
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Фото</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Видео</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Музей</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">ЕБРР</a>
                        </div>
                        <div class="info-menu-item">
                            <a href="#" class="info-menu-link">Конференции и семинары</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="main-container bg-white">
                    <div class="video">
                        <div class="video-title">Социальные ролики ТОО «Водные ресурсы - Маркетинг»</div>
                        <div class="video-list">
                            <div class="video-item">
                                <div class="video-left">
                                    <img src="/img/video.jpg" alt="Video" class="img-responsive">
                                </div>
                                <div class="video-right">
                                    <a class="video-item-link" href="#">Крышки от канализационных люков</a>

                                    <div class="video-item-anons">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                                        exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in
                                        voluptate velit esse cillum.
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="video-item">
                                <div class="video-left">
                                    <img src="/img/video.jpg" alt="Video" class="img-responsive">
                                </div>
                                <div class="video-right">
                                    <a class="video-item-link" href="#">Крышки от канализационных люков</a>

                                    <div class="video-item-anons">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                                        exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in
                                        voluptate velit esse cillum.
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="video-item">
                                <div class="video-left">
                                    <img src="/img/video.jpg" alt="Video" class="img-responsive">
                                </div>
                                <div class="video-right">
                                    <a class="video-item-link" href="#">Крышки от канализационных люков</a>

                                    <div class="video-item-anons">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                                        exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in
                                        voluptate velit esse cillum.
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="video-item">
                                <div class="video-left">
                                    <img src="/img/video.jpg" alt="Video" class="img-responsive">
                                </div>
                                <div class="video-right">
                                    <a class="video-item-link" href="#">Крышки от канализационных люков</a>

                                    <div class="video-item-anons">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                                        exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in
                                        voluptate velit esse cillum.
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="video-item">
                                <div class="video-left">
                                    <img src="/img/video.jpg" alt="Video" class="img-responsive">
                                </div>
                                <div class="video-right">
                                    <a class="video-item-link" href="#">Крышки от канализационных люков</a>

                                    <div class="video-item-anons">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                                        exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in
                                        voluptate velit esse cillum.
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="video-item">
                                <div class="video-left">
                                    <img src="/img/video.jpg" alt="Video" class="img-responsive">
                                </div>
                                <div class="video-right">
                                    <a class="video-item-link" href="#">Крышки от канализационных люков</a>

                                    <div class="video-item-anons">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                                        exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in
                                        voluptate velit esse cillum.
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="video-item">
                                <div class="video-left">
                                    <img src="/img/video.jpg" alt="Video" class="img-responsive">
                                </div>
                                <div class="video-right">
                                    <a class="video-item-link" href="#">Крышки от канализационных люков</a>

                                    <div class="video-item-anons">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod tempor incididunt ut labore et dolore
                                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                                        exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in
                                        voluptate velit esse cillum.
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<?= \frontend\widgets\CallBack::widget(); ?>

<?= \frontend\widgets\Info::widget(); ?>

<link rel="stylesheet" href="/css/video.css">

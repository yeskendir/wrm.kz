-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Фев 18 2018 г., 19:01
-- Версия сервера: 5.6.24
-- Версия PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `wrmkz`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `controller_id`, `sort_order`, `status`, `created_at`, `updated_at`) VALUES
(4, 0, 0, 1, 1518960779, 1518962649),
(5, 0, 1, 1, 1518975081, 1518975081),
(6, 0, 2, 1, 1518975107, 1518975107),
(7, 0, 1, 1, 1518975133, 1518975133);

-- --------------------------------------------------------

--
-- Структура таблицы `category_description`
--

CREATE TABLE IF NOT EXISTS `category_description` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category_description`
--

INSERT INTO `category_description` (`id`, `category_id`, `language_id`, `name`) VALUES
(1, 3, 0, ''),
(2, 3, 1, ''),
(3, 3, 2, ''),
(10, 4, 0, 'йцук'),
(11, 4, 1, 'фыва'),
(12, 4, 2, 'adsfasdf '),
(13, 5, 0, 'Взаимодействие с международными организациями'),
(14, 5, 1, 'Взаимодействие с международными организациями'),
(15, 5, 2, 'Взаимодействие с международными организациями'),
(16, 6, 0, 'Конференции и семинары'),
(17, 6, 1, ''),
(18, 6, 2, ''),
(19, 7, 0, 'Сотрудничество'),
(20, 7, 1, ''),
(21, 7, 2, '');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1517504528),
('m130524_201442_init', 1517504532);

-- --------------------------------------------------------

--
-- Структура таблицы `partner`
--

CREATE TABLE IF NOT EXISTS `partner` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `partner`
--

INSERT INTO `partner` (`id`, `category_id`, `sort_order`, `image`, `slug`, `status`, `created`, `updated`) VALUES
(1, 4, 2, 'sadf', 'asdf', 1, 1518968051, 1518971441),
(2, 4, 0, '', '', 0, 1518971844, 1518972608),
(3, 5, 1, '', '', 1, 1518975179, 1518975179),
(4, 5, 1, '', '', 1, 1518975252, 1518975258);

-- --------------------------------------------------------

--
-- Структура таблицы `partner_description`
--

CREATE TABLE IF NOT EXISTS `partner_description` (
  `id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `partner_description`
--

INSERT INTO `partner_description` (`id`, `partner_id`, `language_id`, `title`, `description`, `short_description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(4, 1, 0, 'adsf', 'sadf', 'asdf', 'asdf', 'asdf', 'sadf'),
(5, 1, 1, 'фвыа', 'фыва', 'фыва', 'фвыа', 'ыфва', 'фыва'),
(6, 1, 2, 'чясм', 'ясчм', 'ячсм', 'ясчм', 'ячсм', 'ясчм'),
(19, 2, 0, 'фыва', 'фыва', 'фвыа', 'фыва', 'фыва', 'фыва'),
(21, 2, 2, '', '', '', '', '', ''),
(20, 2, 1, '', '', '', '', '', ''),
(22, 3, 0, 'Инвестиции европейского банка реконструкции и развития в обновле', 'Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент  Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент', 'Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент', 'Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент', 'Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент', 'Инвестиции европейского банка реконструкции и развития в обновление основных фондов ВКХ г. Шымкент'),
(23, 3, 1, '', '', '', '', '', ''),
(24, 3, 2, '', '', '', '', '', ''),
(28, 4, 0, 'Делегация Будапештского водоканала', 'Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала Делегация Будапештского водоканала', 'Делегация Будапештского водоканала', 'Делегация Будапештского водоканала', 'Делегация Будапештского водоканала', 'Делегация Будапештского водоканала'),
(29, 4, 1, '', '', '', '', '', ''),
(30, 4, 2, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Yeskendir', 'K7tvgqPtPe8O5dHknRUQGAa9Sof2SsdK', '$2y$13$0kSqAkxpYEqK.zQevxBc7er1lnf92Q9XrJEzVr6g4qMC2HMbmLasG', 'RMouYO-EF7k9nMoPJTPFsIBl3czJEMt6_1517752351', 'yeskendir1993@gmail.com', 10, 1517722068, 1517752351),
(2, 'admin', 'K7tvgqPtPe8O5dHknRUQGAa9Sof2SsdK', '$2y$13$0kSqAkxpYEqK.zQevxBc7er1lnf92Q9XrJEzVr6g4qMC2HMbmLasG', NULL, 'barca17@mail.ru', 10, 1517722533, 1517722533),
(16, 'Yeskendirdd', 'K7tvgqPtPe8O5dHknRUQGAa9Sof2SsdK', '$2y$13$0kSqAkxpYEqK.zQevxBc7er1lnf92Q9XrJEzVr6g4qMC2HMbmLasG', NULL, 'yeskenidf@mail.ru', 1, 1517771072, 1517771072),
(17, 'YeskendirJ', 'K7tvgqPtPe8O5dHknRUQGAa9Sof2SsdK', '$2y$13$0kSqAkxpYEqK.zQevxBc7er1lnf92Q9XrJEzVr6g4qMC2HMbmLasG', NULL, 'yeskendirj@mail.ru', 10, 1517771117, 1517771117),
(18, 'YeskendirJa', 'K7tvgqPtPe8O5dHknRUQGAa9Sof2SsdK', '$2y$13$0kSqAkxpYEqK.zQevxBc7er1lnf92Q9XrJEzVr6g4qMC2HMbmLasG', NULL, 'yeskendir@mail.ru', 1, 1517772110, 1517772110);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `category_description`
--
ALTER TABLE `category_description`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `partner_description`
--
ALTER TABLE `partner_description`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `category_description`
--
ALTER TABLE `category_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `partner_description`
--
ALTER TABLE `partner_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<? include_once 'header.php'; ?>
<div id="content">
    <div class="main-carousel">
        <div class="owl-main owl-carousel owl-theme">
            <div class="item">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
            </div>
            <div class="item">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
            </div>
            <div class="item">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
            </div>
            <div class="item">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
            </div>
            <div class="item">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
            </div>
            <div class="item">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
            </div>
            <div class="item">
                <img src="/img/demo-banner.png" alt="20 лет водоканалу" class="img-responsive">
            </div>
        </div>
    </div>
    <div class="company">
        <h1 class="company-title text-center">ТОО «Водные ресурсы-Маркетинг»</h1>

        <div class="company-desc text-center text-uppercase">Чистая вода - источник жизни</div>
        <div class="company-logo"><img src="/img/company-logo.png" alt="ТОО «Водные ресурсы-Маркетинг»"
                                       class="img-responsive center-block"></div>
    </div>
    <div class="service">
        <div class="service-title text-center text-uppercase">Услуги и сервисы</div>
        <div class="container">
            <div class="owl-service owl-carousel owl-theme">
                <div class="item">
                    <img src="/img/banner-service-1.png" class="img-responsive">
                </div>
                <div class="item">
                    <img src="/img/banner-service-2.png" class="img-responsive">
                </div>
                <div class="item">
                    <img src="/img/banner-service-3.png" class="img-responsive">
                </div>
                <div class="item">
                    <img src="/img/banner-service-2.png" class="img-responsive">
                </div>
                <div class="item">
                    <img src="/img/banner-service-3.png" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
    <div class="check">
        <img src="/img/check-img.png" alt="" class="check-img img-responsive">
    </div>

    <div class="info">
        <div class="container">
            <div class="row">
                <div class="info-left col-md-3 col-sm-12">
                    <div class="even">
                        <div class="even-title">события <a href="#" class="even-yet-link">Ещё</a></div>
                        <div class="even-list">
                            <div class="even-item">
                                <a href="#" class="even-item-link">
                                    <img src="/img/even-img-1.png" alt="Реконструкция водопроводной сети"
                                         class="img-responsive even-item-img">

                                    <div class="even-item-anons">
                                        <div class="even-item-title">Реконструкция водопроводной сети</div>
                                        <div class="even-item-date">10.10.2017</div>
                                    </div>
                                </a>
                            </div>

                            <div class="even-item">
                                <a href="#" class="even-item-link">
                                    <img src="/img/even-img-2.png" alt="Об изменениях тарифов в г. Шымкенте"
                                         class="img-responsive even-item-img">

                                    <div class="even-item-anons">
                                        <div class="even-item-title">Об изменениях тарифов в г. Шымкенте</div>
                                        <div class="even-item-date">10.10.2017</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="info-center col-md-6 col-sm-12">
                    <div class="news">
                        <div class="news-title">новости <a href="#" class="news-yet-link">Ещё</a></div>

                        <div class="news-list">
                            <div class="row">
                                <div class="col-md-12 news-top">
                                    <div class="news-item news-item-top">
                                        <a href="#" class="news-item-link">
                                            <div class="news-item-top-left">
                                                <img src="/img/news-img-1.png" alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-img">
                                            </div>
                                            <div class="news-item-top-right">
                                                <div class="news-item-top-anons">
                                                    <div class="news-item-top-title">Капитальный ремонт</div>
                                                    <div class="news-item-top-text">В октябре 2017 года произведен
                                                        капитальный
                                                        ремонт водопроводной сети диаметром 80 мм
                                                        в доме 42 в мкр.Терискей...
                                                    </div>
                                                    <div class="news-item-top-date">10.10.2017</div>
                                                </div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 news-left">
                                    <div class="news-item news-item-left">
                                        <a href="#" class="news-item-link">
                                            <img src="/img/news-img-2.png" alt="Реконструкция водопроводной сети"
                                                 class="img-responsive news-item-left-img hidden-sm hidden-xs">

                                            <img src="/img/news-img-1.png" alt="Реконструкция водопроводной сети"
                                                 class="img-responsive news-item-left-img news-item-left-img-mob hidden-lg hidden-md">

                                            <div class="news-item-left-anons">
                                                <div class="news-item-left-title">Квартком Даулеталы Мыктыбай</div>
                                                <div class="news-item-left-text">Работаю совместно с ТОО «Водные
                                                    ресурсы-Маркетинг» со дня избрания меня председателем квартального
                                                    ...
                                                </div>
                                                <div class="news-item-left-date">04.09.2017</div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-6 news-right">
                                    <div class="news-item news-item-right">
                                        <a href="#" class="news-item-link">
                                            <div class="news-item-right-top">
                                                <img src="/img/news-img-3.png"
                                                     alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-right-img hidden-sm hidden-xs">

                                                <img src="/img/news-img-1.png"
                                                     alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-right-img  news-item-right-img-mob  hidden-lg hidden-md">
                                            </div>
                                            <div class="news-item-right-bottom">
                                                <div class="news-item-right-anons">
                                                    <div class="news-item-right-title">Благодарственное письмо</div>
                                                    <div class="news-item-right-text">Благодарим водоканал отдел
                                                        канализации
                                                        за хорошую работу за быструю ...
                                                    </div>
                                                    <div class="news-item-right-date">10.08.2017</div>
                                                </div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="info-right col-md-3 col-sm-12">
                    <div class="archive">
                        <div class="archive-title">Архив <a href="#" class="archive-yet-link">Ещё</a></div>

                        <div class="archive-list">
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="data">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-12 data-left">
                    <div class="data-left-title text-uppercase text-black">Информация для абонентов</div>
                    <div class="data-left-box bg-white">
                        <div class="row">
                            <div class="col-sm-6">

                                <div class="data-par">
                                    <div class="data-par-title text-uppercase"><span class="icon-home"></span>обслуживание<span
                                            class="icon-arrow-right"></span></div>
                                    <div class="data-list">
                                        <div class="data-item"><a href="#">Вопросы-ответы</a></div>
                                        <div class="data-item"><a href="#">Сообщение для абонентов</a></div>
                                        <div class="data-item"><a href="#">Обслуживание приборов учета воды</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="data-par">
                                    <div class="data-par-title text-uppercase"><span class="icon-document"></span>документация<span
                                            class="icon-arrow-right"></span></div>
                                    <div class="data-list">
                                        <div class="data-item"><a href="#">Отчеты перед потребителем</a></div>
                                        <div class="data-item"><a href="#">Тарифы</a></div>
                                        <div class="data-item"><a href="#">Порядок исполнения обращений</a></div>
                                        <div class="data-item"><a href="#">Порядок подключения к сетям водопровода и
                                            канализации</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 data-right">
                    <div class="data-right-title  text-uppercase text-black">ГЕНЕРАЛЬНый ДИРЕКТОР ТОО «Водные
                        ресурсы-маркетинг»
                    </div>
                    <img src="/img/boss.png" alt="УРМАНОВ Мурад Анарбекович"
                         class="data-right-img img-responsive center-block">

                    <div class="data-boss text-uppercase text-center">УРМАНОВ Мурад Анарбекович</div>
                    <div class="data-right-buttons text-center">
                        <div class="btn btn-info">Задать вопрос</div>
                        <div class="btn btn-primary">Опрос потребителей</div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="call">
        <div class="container text-white">
            <div class="call-title text-uppercase text-center">обратная связь</div>
            <div class="row">
                <form>
                    <div class="col-sm-6 call-left">
                        <div class="form-group">
                            <label for="callInputName" class="text-uppercase">Ф.И.О.:<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="callInputName" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="callInputPhone">Телефон:<span class="text-danger">*</span></label>
                            <input type="tel" class="form-control" id="callInputPhone">
                        </div>
                        <div class="form-group">
                            <label for="callInputEmail">Email:</label>
                            <input type="email" class="form-control" id="callInputEmail">
                        </div>
                    </div>
                    <div class="col-sm-6 call-right">
                        <div class="form-group">
                            <label for="callInputComment">Комментарий:</label>
                            <textarea id="callInputComment" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <button type="submit" class="btn btn-info btn-call-back">Отправить</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


</div>
<? include_once 'footer.php'; ?>


<!-- Modal Sign up -->
<div class="modal fade modal-custom modal-signup" id="signUpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-signup-logo text-center text-uppercase">
                    <img src="/img/company-logo.png" alt="ТОО «Водные ресурсы-Маркетинг»" class="img-responsive modal-signup-logo-img">
                    <span class="modal-signup-logo-text">Вход в систему</span>
                </div>

                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Логин">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Пароль">
                    </div>
                    <div class="form-group form-button">
                        <button type="submit" class="btn btn-primary">Войти</button>
                        <div class="modal-signup-auth">
                            <a href="#" class="modal-signup-auth-link">Забыли логин или пароль?</a>
                            <a href="#" class="modal-signup-auth-link">Зарегистрироваться</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#signUpModal').modal('show');
    });
</script>

<!-- Owl Stylesheets -->
<link rel="stylesheet" href="/js/owl/assets/owl.carousel.min.css">
<link rel="stylesheet" href="/js/owl/assets/owl.theme.default.min.css">
<script src="/js/owl/owl.carousel.js"></script>
<script>
    jQuery(document).ready(function ($) {
        $('.owl-main').owlCarousel({
            items: 1,
            animateOut: 'fadeOut',
            loop: true,
            margin: 10
        });
        $('.owl-service').owlCarousel({
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    margin: 20
                },
                480: {
                    items: 2,
                    margin: 20
                },
                600: {
                    items: 3,
                    margin: 20
                },
                1000: {
                    items: 3,
                    margin: 200
                }
            },
            dots: false,
            nav: true,
            navText: ["<img src='/img/icon-caret-left.png' class='img-responsive'>", "<img src='/img/icon-caret-right.png' class='img-responsive'>"],
            loop: true,
        });
    });
</script>
<!-- Owl Stylesheets -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- Style -->
    <link rel="stylesheet" href="css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header class="header">
    <div class="header-top">
        <div class="container">
            <ul class="nav nav-pills header-nav-pills">
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle header-top-link" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="true"
                       aria-expanded="false">
                        О компании <span class="caret-custom"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>
                <li><a href="#" class="header-top-link">закупки</a></li>
                <li><a href="#" class="header-top-link">Контакты</a></li>
            </ul>
            <div class="header-top-search">
                <div class="search-input-group">
                    <input class="form-control" placeholder="Поиск на сайте">
                    <button class="btn">
                        <img src="/img/icon-search.png" alt="Пойск" class="img-responsive">
                    </button>
                </div>
            </div>
            <ul class="nav nav-pills header-nav-tools">
                <li class="tools-item">
                    <a href="#" class="tools-link">
                        <img src="/img/icon-share.png" alt="Поделиться" class="img-responsive">
                    </a>
                </li>
                <li class="tools-item">
                    <a href="#" class="tools-link">
                        <img src="/img/icon-map.png" alt="Карта сайта" class="img-responsive">
                    </a>
                </li>
                <li role="presentation" class="dropdown dropdown-lang">
                    <a class="dropdown-toggle text-white" data-toggle="dropdown" href="#"
                       role="button"
                       aria-haspopup="true" aria-expanded="false">
                        Рус<span class="caret-custom"></span>
                    </a>
                    <ul class="dropdown-menu text-white">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>
                <li class="tools-item tools-cabinet"><a href="#" class="text-white"><img src="/img/icon-user.png"
                                                                                         alt="Личный кабинет"
                                                                                         class="img-responsive tools-cabinet-img"><span
                        class="hidden-sm tools-cabinet-title">Личный кабинет</span> <span class="clearfix"></span></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="header-main">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only"><img src="/img/logo.png" alt="Logo" class="img-responsive"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="/img/logo.png" alt="Logo" class="img-responsive">
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="clearfix hidden-lg hidden-md hidden-xs"></div>
                    <ul class="nav navbar-nav">
                        <li><a href="#">Отчеты</a></li>
                        <li><a href="#">Потребителю</a></li>
                        <li><a href="#">Галерея</a></li>
                        <li><a href="#">Новости</a></li>
                    </ul>
                    <div class="navbar-right navbar-info">
                        <div class="navbar-info-time">звоните нам: пн. - сб. 9:00 – 18:00</div>
                        <div class="navbar-info-phone">+7 (7252) 321-275</div>
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>
</header>

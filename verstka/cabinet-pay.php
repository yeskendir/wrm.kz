<? include_once 'header.php'; ?>
<div id="content">
    <div class="navigation">
        <div class="container">
            <ol class="breadcrumb navigation-breadcrumb">
                <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
                <li class="active">Личный кабинет</li>
            </ol>
            <h1 class="text-uppercase navigation-title">личный кабинет</h1>
        </div>
    </div>


    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="info-menu">
                        <div class="info-menu-title text-uppercase">Литошенко Олег</div>
                        <div class="info-menu-list">
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Личные данные</a>
                            </div>
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Печать квитанции</a>
                            </div>
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Показания учета</a>
                            </div>
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Оплатить за воду</a>
                            </div>
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Выход</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="main-container">
                        <div class="report">
                            <div class="report-date bg-white">
                                <div class="text-uppercase report-title text-black">ОПЛАТА ЗА ВОДУ</div>
                                <div class="pay-kassa">Наличными через кассу</div>

                                <div class="table-responsive">
                                    <table class="table table-striped table-report">
                                        <tr>
                                            <th>Место расположение</th>
                                            <th>Телефон</th>
                                            <th>Режим работы</th>
                                            <th>Выходные</th>
                                        </tr>
                                        <tr>
                                            <td>3 мкр дом б/н,(возле торгдома "Баянсулу")</td>
                                            <td>28-44-12</td>
                                            <td>9.00-19.00</td>
                                            <td>без выходных</td>
                                        </tr>
                                        <tr>
                                            <td>3 мкр дом б/н,(возле торгдома "Баянсулу")</td>
                                            <td>28-44-12</td>
                                            <td>9.00-19.00</td>
                                            <td>без выходных</td>
                                        </tr>
                                        <tr>
                                            <td>3 мкр дом б/н,(возле торгдома "Баянсулу")</td>
                                            <td>28-44-12</td>
                                            <td>9.00-19.00</td>
                                            <td>без выходных</td>
                                        </tr>
                                        <tr>
                                            <td>3 мкр дом б/н,(возле торгдома "Баянсулу")</td>
                                            <td>28-44-12</td>
                                            <td>9.00-19.00</td>
                                            <td>без выходных</td>
                                        </tr>
                                        <tr>
                                            <td>3 мкр дом б/н,(возле торгдома "Баянсулу")</td>
                                            <td>28-44-12</td>
                                            <td>9.00-19.00</td>
                                            <td>без выходных</td>
                                        </tr>
                                    </table>
                                </div>
                                <p class="text-uppercase pay-attention">ВНИМАНИЕ! Обеденный перерыв с13.00 до14.00</p>
                                <div class="qiwi">
                                    <div class="qiwi-title">Оплата через QIWI Терминалы</div>
                                    <div class="qiwi-description">
                                        <img src="/img/qiwi.jpg" alt="" class="img-responsive qiwi-img">
                                        Оплатить наши услуги можно в любом QIWI Терминале по пути домой. QIWI Терминалы легко узнать по трем большим кнопкам на экране. Заплатить просто: достаточно нажать 3-5 кнопок и внести наличные. Деньги будут зачислены моментально. Карта расположения терминалов и контактная информация
                                        <a href="#">на сайте</a>
                                    </div>
                                </div>
                                <div class="bank">
                                    <div class="bank-title">Оплата через банк</div>
                                    <div class="bank-description">Вы можете оплатить услуги ТОО "Водные ресурсы Маркетинг" через любой из нижеперечисленных банков.
                                        Для оплаты при себе необходимо иметь квитанцию на оплату.</div>
                                    <div class="bank-list">
                                        <div class="bank-item">
                                            <img src="/img/bank-senim.jpg" alt="Сен1м банк" class="img-responsive">
                                        </div>
                                        <div class="bank-item">
                                            <img src="/img/bank-halyk.jpg" alt="halyk" class="img-responsive">
                                        </div>
                                        <div class="bank-item">
                                            <img src="/img/bank-sesna.jpg" alt="sesna" class="img-responsive">
                                        </div>
                                        <div class="bank-item">
                                            <img src="/img/bank-temir.jpg" alt="temir" class="img-responsive">
                                        </div>
                                        <div class="bank-item">
                                            <img src="/img/bank-euras.jpg" alt="euras" class="img-responsive">
                                        </div>
                                        <div class="bank-item">
                                            <img src="/img/bank-alians.jpg" alt="alinas" class="img-responsive">
                                        </div>
                                        <div class="bank-item">
                                            <img src="/img/bank-atf.jpg" alt="atf" class="img-responsive">
                                        </div>
                                        <div class="bank-item">
                                            <img src="/img/bank-bta.jpg" alt="bta" class="img-responsive">
                                        </div>
                                        <div class="bank-item">
                                            <img src="/img/bank-rbk.jpg" alt="rbk" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
                                <div class="kazkom">
                                    <div class="kazkom-title">Платежной картой через интернет</div>
                                    <div class="kazkom-description">
                                        Вы можете оплатить наши услуги через специальный интернет сайты, используя платежную карту.
                                        На сайте Казкоммерцбанка <a href="#">перейти</a>
                                        Для осуществления платежа у Вас должна быть платежная карта Казкоммерцбанка.
                                        Чтобы начать, Вы должны зарегистрировать свою карту для осуществления платежа - регистрировать - перейти
                                        Дальнейшие инструкции Вы найдете на сайте Казкоммерцбанка.
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="call">
        <div class="container text-white">
            <div class="call-title text-uppercase text-center">обратная связь</div>
            <div class="row">
                <form>
                    <div class="col-sm-6 call-left">
                        <div class="form-group">
                            <label for="callInputName" class="text-uppercase">Ф.И.О.:<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="callInputName" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="callInputPhone">Телефон:<span class="text-danger">*</span></label>
                            <input type="tel" class="form-control" id="callInputPhone">
                        </div>
                        <div class="form-group">
                            <label for="callInputEmail">Email:</label>
                            <input type="email" class="form-control" id="callInputEmail">
                        </div>
                    </div>
                    <div class="col-sm-6 call-right">
                        <div class="form-group">
                            <label for="callInputComment">Комментарий:</label>
                            <textarea id="callInputComment" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <button type="submit" class="btn btn-info btn-call-back">Отправить</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <div class="info">
        <div class="container">
            <div class="row">
                <div class="info-left col-md-3 col-sm-12">
                    <div class="even">
                        <div class="even-title">события <a href="#" class="even-yet-link">Ещё</a></div>
                        <div class="even-list">
                            <div class="even-item">
                                <a href="#" class="even-item-link">
                                    <img src="/img/even-img-1.png" alt="Реконструкция водопроводной сети"
                                         class="img-responsive even-item-img">

                                    <div class="even-item-anons">
                                        <div class="even-item-title">Реконструкция водопроводной сети</div>
                                        <div class="even-item-date">10.10.2017</div>
                                    </div>
                                </a>
                            </div>

                            <div class="even-item">
                                <a href="#" class="even-item-link">
                                    <img src="/img/even-img-2.png" alt="Об изменениях тарифов в г. Шымкенте"
                                         class="img-responsive even-item-img">

                                    <div class="even-item-anons">
                                        <div class="even-item-title">Об изменениях тарифов в г. Шымкенте</div>
                                        <div class="even-item-date">10.10.2017</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="info-center col-md-6 col-sm-12">
                    <div class="news">
                        <div class="news-title">новости <a href="#" class="news-yet-link">Ещё</a></div>

                        <div class="news-list">
                            <div class="row">
                                <div class="col-md-12 news-top">
                                    <div class="news-item news-item-top">
                                        <a href="#" class="news-item-link">
                                            <div class="news-item-top-left">
                                                <img src="/img/news-img-1.png" alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-img">
                                            </div>
                                            <div class="news-item-top-right">
                                                <div class="news-item-top-anons">
                                                    <div class="news-item-top-title">Капитальный ремонт</div>
                                                    <div class="news-item-top-text">В октябре 2017 года произведен
                                                        капитальный
                                                        ремонт водопроводной сети диаметром 80 мм
                                                        в доме 42 в мкр.Терискей...
                                                    </div>
                                                    <div class="news-item-top-date">10.10.2017</div>
                                                </div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 news-left">
                                    <div class="news-item news-item-left">
                                        <a href="#" class="news-item-link">
                                            <img src="/img/news-img-2.png" alt="Реконструкция водопроводной сети"
                                                 class="img-responsive news-item-left-img hidden-sm hidden-xs">

                                            <img src="/img/news-img-1.png" alt="Реконструкция водопроводной сети"
                                                 class="img-responsive news-item-left-img news-item-left-img-mob hidden-lg hidden-md">

                                            <div class="news-item-left-anons">
                                                <div class="news-item-left-title">Квартком Даулеталы Мыктыбай</div>
                                                <div class="news-item-left-text">Работаю совместно с ТОО «Водные
                                                    ресурсы-Маркетинг» со дня избрания меня председателем квартального
                                                    ...
                                                </div>
                                                <div class="news-item-left-date">04.09.2017</div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-6 news-right">
                                    <div class="news-item news-item-right">
                                        <a href="#" class="news-item-link">
                                            <div class="news-item-right-top">
                                                <img src="/img/news-img-3.png"
                                                     alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-right-img hidden-sm hidden-xs">

                                                <img src="/img/news-img-1.png"
                                                     alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-right-img  news-item-right-img-mob  hidden-lg hidden-md">
                                            </div>
                                            <div class="news-item-right-bottom">
                                                <div class="news-item-right-anons">
                                                    <div class="news-item-right-title">Благодарственное письмо</div>
                                                    <div class="news-item-right-text">Благодарим водоканал отдел
                                                        канализации
                                                        за хорошую работу за быструю ...
                                                    </div>
                                                    <div class="news-item-right-date">10.08.2017</div>
                                                </div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="info-right col-md-3 col-sm-12">
                    <div class="archive">
                        <div class="archive-title">Архив <a href="#" class="archive-yet-link">Ещё</a></div>

                        <div class="archive-list">
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<? include_once 'footer.php'; ?>

<link rel="stylesheet" href="/css/cabinet-data.css">

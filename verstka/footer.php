<footer class="footer">
    <div class="footer-company bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-company-item">
                        <div class="footer-company-left">
                            <img src="/img/icon-kz.png" alt="Официальный сайт президента Республики казахстан"
                                 class="img-responsive footer-company-img">
                        </div>
                        <div class="footer-company-right">
                            <div class="footer-company-name">Официальный сайт президента Республики казахстан</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-company-item">
                        <div class="footer-company-left">
                            <img src="/img/icon-egov.png" alt="Государственные услуги и информация онлайн"
                                 class="img-responsive footer-company-img">

                        </div>

                        <div class="footer-company-right">
                            <div class="footer-company-name">Государственные услуги и информация онлайн</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-company-item">
                        <div class="footer-company-left">
                            <img src="/img/icon-hz.png" alt="План нации - 100 конкретных шагов"
                                 class="img-responsive footer-company-img">
                        </div>

                        <div class="footer-company-right">
                            <div class="footer-company-name">План нации - 100 конкретных шагов</div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-company-item">
                        <div class="footer-company-left">
                            <img src="/img/icon-shymkent.png" alt="Официальный интернет ресурс Акимата города Шымкент"
                                 class="img-responsive footer-company-img">
                        </div>
                        <div class="footer-company-right">
                            <div class="footer-company-name">Официальный интернет ресурс Акимата города Шымкент</div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>

    </div>
    <div class="footer-info">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="footer-nav">
                        <div class="footer-nav-list">
                            <div class="footer-nav-item"><a href="#" class="footer-nav-link">О компании</a></div>
                            <div class="footer-nav-item"><a href="#" class="footer-nav-link">Потребителю</a></div>
                            <div class="footer-nav-item"><a href="#" class="footer-nav-link">Галерея</a></div>
                        </div>
                        <div class="footer-nav-list">
                            <div class="footer-nav-item"><a href="#" class="footer-nav-link">Отчеты</a></div>
                            <div class="footer-nav-item"><a href="#" class="footer-nav-link">Закупки</a></div>
                            <div class="footer-nav-item"><a href="#" class="footer-nav-link">Новости</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="social text-center">
                        <a href="#" class="social-link">
                            <img src="/img/icon-vk.png" alt="vk.com">
                        </a>
                        <a href="#" class="social-link">
                            <img src="/img/icon-tw.png" alt="tw.com">
                        </a>
                        <a href="#" class="social-link">
                            <img src="/img/icon-inst.png" alt="inst.com">
                        </a>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="address">
                                <div class="address-text">Республика Казахстан, г. Шымкент,<br> Адрес: г.Шымкент, ул.
                                    Г.Орманова 17
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="call-phone">
                                <div class="call-phone-time text-uppercase">звоните нам: пн. - СБ. 9:00 – 18:00</div>
                                <a class="call-phone-link" href="tel:77252321275">+7 (7252) 321-275</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row footer-bottom">
                <div class="col-md-5">
                    <div class="footer-bottom-site">wrm.kz  © WMR,2013 - 2017 гг. </div>
                </div>
                <div class="col-md-2">
                    <img src="/img/footer-logo.png" alt="Logo" class="img-responsive center-block">
                </div>
                <div class="col-md-5">
                    <a href="#" class="footer-created-link">Разработано в «Media Rocket»</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

</body>
</html>
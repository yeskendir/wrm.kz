<? include_once 'header.php'; ?>
<div id="content">
    <div class="navigation">
        <div class="container">
            <ol class="breadcrumb navigation-breadcrumb">
                <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
                <li class="active">Корпоративное управление</li>
            </ol>
            <h1 class="text-uppercase navigation-title">Корпоративное управление</h1>
        </div>
    </div>


    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="info-menu">
                        <div class="info-menu-title text-uppercase">отчеты</div>
                        <div class="info-menu-list">
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Корпоративное управление</a>
                            </div>
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Тарифная смета</a>
                            </div>
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Итоги внутреннего аудита</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="main-container">
                        <div class="report">
                            <div class="report-date bg-white">
                                <form class="form-inline form-date">
                                    <div class="form-group">
                                        <label for="inputDateStart">Дата с:</label>
                                        <input type="date" class="form-control" id="inputDateStart">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputDateEnd">по:</label>
                                        <input type="date" class="form-control" id="inputDateEnd">
                                    </div>
                                    <button type="submit" class="btn btn-info btn-date">Применить</button>
                                    <div class="clearfix"></div>
                                </form>

                                <div class="table-responsible">
                                    <table class="table table-striped table-report">
                                        <tr>
                                            <th>Документ</th>
                                            <th>Скачать</th>
                                            <th>Размер</th>
                                        </tr>
                                        <tr>
                                            <td>Бухгалтерский баланс</td>
                                            <td><a href="#"><span class="icon-pdf"></span></a></td>
                                            <td>1.47 MB</td>
                                        </tr>
                                        <tr>
                                            <td>Расшифровка к бухгалтерскому балансу по дебиторской и кредиторской задолженности </td>
                                            <td><a href="#"><span class="icon-pdf"></span></a></td>
                                            <td>2.34 MB</td>
                                        </tr>
                                        <tr>
                                            <td>Отчет о прибылях и убытках (Ф. 3) </td>
                                            <td><a href="#"><span class="icon-word"></span></a></td>
                                            <td>51 KB</td>
                                        </tr>
                                        <tr>
                                            <td>Отчет о прибылях и убытках (приложение 3) </td>
                                            <td><a href="#"><span class="icon-all-file"></span></a></td>
                                            <td>18 KB</td>
                                        </tr>
                                        <tr>
                                            <td>Отчет об изменениях в капитале </td>
                                            <td><a href="#"><span class="icon-all-file"></span></a></td>
                                            <td>51 KB</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="call">
        <div class="container text-white">
            <div class="call-title text-uppercase text-center">обратная связь</div>
            <div class="row">
                <form>
                    <div class="col-sm-6 call-left">
                        <div class="form-group">
                            <label for="callInputName" class="text-uppercase">Ф.И.О.:<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="callInputName" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="callInputPhone">Телефон:<span class="text-danger">*</span></label>
                            <input type="tel" class="form-control" id="callInputPhone">
                        </div>
                        <div class="form-group">
                            <label for="callInputEmail">Email:</label>
                            <input type="email" class="form-control" id="callInputEmail">
                        </div>
                    </div>
                    <div class="col-sm-6 call-right">
                        <div class="form-group">
                            <label for="callInputComment">Комментарий:</label>
                            <textarea id="callInputComment" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <button type="submit" class="btn btn-info btn-call-back">Отправить</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <div class="info">
        <div class="container">
            <div class="row">
                <div class="info-left col-md-3 col-sm-12">
                    <div class="even">
                        <div class="even-title">события <a href="#" class="even-yet-link">Ещё</a></div>
                        <div class="even-list">
                            <div class="even-item">
                                <a href="#" class="even-item-link">
                                    <img src="/img/even-img-1.png" alt="Реконструкция водопроводной сети"
                                         class="img-responsive even-item-img">

                                    <div class="even-item-anons">
                                        <div class="even-item-title">Реконструкция водопроводной сети</div>
                                        <div class="even-item-date">10.10.2017</div>
                                    </div>
                                </a>
                            </div>

                            <div class="even-item">
                                <a href="#" class="even-item-link">
                                    <img src="/img/even-img-2.png" alt="Об изменениях тарифов в г. Шымкенте"
                                         class="img-responsive even-item-img">

                                    <div class="even-item-anons">
                                        <div class="even-item-title">Об изменениях тарифов в г. Шымкенте</div>
                                        <div class="even-item-date">10.10.2017</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="info-center col-md-6 col-sm-12">
                    <div class="news">
                        <div class="news-title">новости <a href="#" class="news-yet-link">Ещё</a></div>

                        <div class="news-list">
                            <div class="row">
                                <div class="col-md-12 news-top">
                                    <div class="news-item news-item-top">
                                        <a href="#" class="news-item-link">
                                            <div class="news-item-top-left">
                                                <img src="/img/news-img-1.png" alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-img">
                                            </div>
                                            <div class="news-item-top-right">
                                                <div class="news-item-top-anons">
                                                    <div class="news-item-top-title">Капитальный ремонт</div>
                                                    <div class="news-item-top-text">В октябре 2017 года произведен
                                                        капитальный
                                                        ремонт водопроводной сети диаметром 80 мм
                                                        в доме 42 в мкр.Терискей...
                                                    </div>
                                                    <div class="news-item-top-date">10.10.2017</div>
                                                </div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 news-left">
                                    <div class="news-item news-item-left">
                                        <a href="#" class="news-item-link">
                                            <img src="/img/news-img-2.png" alt="Реконструкция водопроводной сети"
                                                 class="img-responsive news-item-left-img hidden-sm hidden-xs">

                                            <img src="/img/news-img-1.png" alt="Реконструкция водопроводной сети"
                                                 class="img-responsive news-item-left-img news-item-left-img-mob hidden-lg hidden-md">

                                            <div class="news-item-left-anons">
                                                <div class="news-item-left-title">Квартком Даулеталы Мыктыбай</div>
                                                <div class="news-item-left-text">Работаю совместно с ТОО «Водные
                                                    ресурсы-Маркетинг» со дня избрания меня председателем квартального
                                                    ...
                                                </div>
                                                <div class="news-item-left-date">04.09.2017</div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-6 news-right">
                                    <div class="news-item news-item-right">
                                        <a href="#" class="news-item-link">
                                            <div class="news-item-right-top">
                                                <img src="/img/news-img-3.png"
                                                     alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-right-img hidden-sm hidden-xs">

                                                <img src="/img/news-img-1.png"
                                                     alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-right-img  news-item-right-img-mob  hidden-lg hidden-md">
                                            </div>
                                            <div class="news-item-right-bottom">
                                                <div class="news-item-right-anons">
                                                    <div class="news-item-right-title">Благодарственное письмо</div>
                                                    <div class="news-item-right-text">Благодарим водоканал отдел
                                                        канализации
                                                        за хорошую работу за быструю ...
                                                    </div>
                                                    <div class="news-item-right-date">10.08.2017</div>
                                                </div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="info-right col-md-3 col-sm-12">
                    <div class="archive">
                        <div class="archive-title">Архив <a href="#" class="archive-yet-link">Ещё</a></div>

                        <div class="archive-list">
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<? include_once 'footer.php'; ?>

<link rel="stylesheet" href="/css/report.css">

<? include_once 'header.php'; ?>
<div id="content">
    <div class="navigation">
        <div class="container">
            <ol class="breadcrumb navigation-breadcrumb">
                <li><a href="#" class="navigation-breadcrumb-link">Главная</a></li>
                <li class="active">Структура</li>
            </ol>
            <h1 class="text-uppercase navigation-title">СТРУКТУРА</h1>
        </div>
    </div>


    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="info-menu">
                        <div class="info-menu-title text-uppercase">потребителю</div>
                        <div class="info-menu-list">
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Вопросы-ответы</a>
                            </div>
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Информация для абонентов</a>
                            </div>
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Отчеты перед потребителем</a>
                            </div>
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Тарифы</a>
                            </div>
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Опрос потребителей</a>
                            </div>
                            <div class="info-menu-item">
                                <a href="#" class="info-menu-link">Запрос задолженности</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="main-container">
                        <div class="report">
                            <div class="report-date bg-white">
                                <p>В соответствии с приказом Департамента Комитета по регулированию естественных монополий и защите конкуренции
                                    Министерства национальной экономики Республики Казахстан по ЮКО № 179-ОД от 16 июня 2016 года
                                    ТОО «Водные ресурсы-Маркетинг» уведомляет своих потребителей, что тарифы с 1 июля 2017 года по 1 июля 2018 года за 1м3
                                    по оказанию услуг подачи воды по распределительным сетям и отводу сточных вод с вводом в действие 01 июля 2017 года
                                    установлены нижеследующие:</p>
                                <br>
                                <br>
                                <div class="text-uppercase report-title text-center">Услуги подачи воды по распределительным сетям</div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-report">
                                        <tr>
                                            <th>п/н</th>
                                            <th>Группа потребителей</th>
                                            <th>без НДС</th>
                                            <th>c НДС</th>
                                        </tr>
                                        <tr>
                                            <td>1 гр</td>
                                            <td>Населения и теплоснабжающие предприятия</td>
                                            <td>78,10</td>
                                            <td>87,47</td>
                                        </tr>
                                        <tr>
                                            <td>2 гр</td>
                                            <td>Государственные организации</td>
                                            <td>345,20</td>
                                            <td>386,62</td>
                                        </tr>
                                        <tr>
                                            <td>3 гр</td>
                                            <td>Прочие потребители не входящие в 1,2 группу</td>
                                            <td>267,25</td>
                                            <td>299,32</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="text-uppercase report-title text-center">Услуги по отводу сточных вод</div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-report">
                                        <tr>
                                            <th>п/н</th>
                                            <th>Группа потребителей</th>
                                            <th>без НДС</th>
                                            <th>c НДС</th>
                                        </tr>
                                        <tr>
                                            <td>1 гр</td>
                                            <td>Населения и теплоснабжающие предприятия</td>
                                            <td>78,10</td>
                                            <td>87,47</td>
                                        </tr>
                                        <tr>
                                            <td>2 гр</td>
                                            <td>Государственные организации</td>
                                            <td>345,20</td>
                                            <td>386,62</td>
                                        </tr>
                                        <tr>
                                            <td>3 гр</td>
                                            <td>Прочие потребители не входящие в 1,2 группу</td>
                                            <td>267,25</td>
                                            <td>299,32</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="call">
        <div class="container text-white">
            <div class="call-title text-uppercase text-center">обратная связь</div>
            <div class="row">
                <form>
                    <div class="col-sm-6 call-left">
                        <div class="form-group">
                            <label for="callInputName" class="text-uppercase">Ф.И.О.:<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="callInputName" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="callInputPhone">Телефон:<span class="text-danger">*</span></label>
                            <input type="tel" class="form-control" id="callInputPhone">
                        </div>
                        <div class="form-group">
                            <label for="callInputEmail">Email:</label>
                            <input type="email" class="form-control" id="callInputEmail">
                        </div>
                    </div>
                    <div class="col-sm-6 call-right">
                        <div class="form-group">
                            <label for="callInputComment">Комментарий:</label>
                            <textarea id="callInputComment" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <button type="submit" class="btn btn-info btn-call-back">Отправить</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <div class="info">
        <div class="container">
            <div class="row">
                <div class="info-left col-md-3 col-sm-12">
                    <div class="even">
                        <div class="even-title">события <a href="#" class="even-yet-link">Ещё</a></div>
                        <div class="even-list">
                            <div class="even-item">
                                <a href="#" class="even-item-link">
                                    <img src="/img/even-img-1.png" alt="Реконструкция водопроводной сети"
                                         class="img-responsive even-item-img">

                                    <div class="even-item-anons">
                                        <div class="even-item-title">Реконструкция водопроводной сети</div>
                                        <div class="even-item-date">10.10.2017</div>
                                    </div>
                                </a>
                            </div>

                            <div class="even-item">
                                <a href="#" class="even-item-link">
                                    <img src="/img/even-img-2.png" alt="Об изменениях тарифов в г. Шымкенте"
                                         class="img-responsive even-item-img">

                                    <div class="even-item-anons">
                                        <div class="even-item-title">Об изменениях тарифов в г. Шымкенте</div>
                                        <div class="even-item-date">10.10.2017</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="info-center col-md-6 col-sm-12">
                    <div class="news">
                        <div class="news-title">новости <a href="#" class="news-yet-link">Ещё</a></div>

                        <div class="news-list">
                            <div class="row">
                                <div class="col-md-12 news-top">
                                    <div class="news-item news-item-top">
                                        <a href="#" class="news-item-link">
                                            <div class="news-item-top-left">
                                                <img src="/img/news-img-1.png" alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-img">
                                            </div>
                                            <div class="news-item-top-right">
                                                <div class="news-item-top-anons">
                                                    <div class="news-item-top-title">Капитальный ремонт</div>
                                                    <div class="news-item-top-text">В октябре 2017 года произведен
                                                        капитальный
                                                        ремонт водопроводной сети диаметром 80 мм
                                                        в доме 42 в мкр.Терискей...
                                                    </div>
                                                    <div class="news-item-top-date">10.10.2017</div>
                                                </div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 news-left">
                                    <div class="news-item news-item-left">
                                        <a href="#" class="news-item-link">
                                            <img src="/img/news-img-2.png" alt="Реконструкция водопроводной сети"
                                                 class="img-responsive news-item-left-img hidden-sm hidden-xs">

                                            <img src="/img/news-img-1.png" alt="Реконструкция водопроводной сети"
                                                 class="img-responsive news-item-left-img news-item-left-img-mob hidden-lg hidden-md">

                                            <div class="news-item-left-anons">
                                                <div class="news-item-left-title">Квартком Даулеталы Мыктыбай</div>
                                                <div class="news-item-left-text">Работаю совместно с ТОО «Водные
                                                    ресурсы-Маркетинг» со дня избрания меня председателем квартального
                                                    ...
                                                </div>
                                                <div class="news-item-left-date">04.09.2017</div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-md-6 news-right">
                                    <div class="news-item news-item-right">
                                        <a href="#" class="news-item-link">
                                            <div class="news-item-right-top">
                                                <img src="/img/news-img-3.png"
                                                     alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-right-img hidden-sm hidden-xs">

                                                <img src="/img/news-img-1.png"
                                                     alt="Реконструкция водопроводной сети"
                                                     class="img-responsive news-item-right-img  news-item-right-img-mob  hidden-lg hidden-md">
                                            </div>
                                            <div class="news-item-right-bottom">
                                                <div class="news-item-right-anons">
                                                    <div class="news-item-right-title">Благодарственное письмо</div>
                                                    <div class="news-item-right-text">Благодарим водоканал отдел
                                                        канализации
                                                        за хорошую работу за быструю ...
                                                    </div>
                                                    <div class="news-item-right-date">10.08.2017</div>
                                                </div>
                                            </div>
                                            <span class="clearfix"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="info-right col-md-3 col-sm-12">
                    <div class="archive">
                        <div class="archive-title">Архив <a href="#" class="archive-yet-link">Ещё</a></div>

                        <div class="archive-list">
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                            <div class="archive-item">
                                <a href="#" class="archive-item-link">
                                    <div class="archive-item-text">Объявление. В некоторых микрорайонах
                                        Шымкента 8 и 9 июня будет
                                        отключена вода
                                    </div>
                                    <div class="archive-item-date">04.09.2017</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<? include_once 'footer.php'; ?>

<link rel="stylesheet" href="/css/structure.css">

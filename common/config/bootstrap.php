<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

if ($_SERVER["REMOTE_ADDR"] == "127.0.0.1") {
    Yii::setAlias('frontendWebroot', 'http:/wrm.local/');
    Yii::setAlias('backendWebroot', 'http://admin.wrm.local/');
} else {
    Yii::setAlias('frontendWebroot', 'http://wrm.mediarocket.kz/');
    Yii::setAlias('backendWebroot', 'http://admin.wrm.mediarocket.kz/');
}
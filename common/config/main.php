<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];

function dump($data)
{
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
}

function pr($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

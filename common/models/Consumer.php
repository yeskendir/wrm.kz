<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "consumer".
 *
 * @property int $id
 * @property int $sort_order
 * @property string $image
 * @property string $slug
 * @property int $status
 * @property int $created
 * @property int $updated
 */
class Consumer extends \yii\db\ActiveRecord
{
    public $title;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consumer';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'updated'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated'],
                ],
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['sort_order', 'image', 'slug', 'status', 'created', 'updated'], 'required'],
            [['sort_order', 'created', 'updated'], 'integer'],
            [['image', 'slug'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
            ['sort_order', 'default', 'value' => 0],
            ['image', 'default', 'value' => ''],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'image' => Yii::t('app', 'Image'),
            'slug' => Yii::t('app', 'Slug'),
            'status' => Yii::t('app', 'Status'),
            'created' => Yii::t('app', 'Created'),
            'updated' => Yii::t('app', 'Updated'),
        ];
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article_description".
 *
 * @property int $id
 * @property int $article_id
 * @property int $language_id
 * @property string $title
 * @property string $description
 * @property string $short_description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 */
class ArticleDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['article_id', 'language_id', 'title', 'description', 'short_description', 'meta_title', 'meta_description', 'meta_keyword'], 'required'],
            [['article_id', 'language_id'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 64],
            [['short_description', 'meta_title', 'meta_description', 'meta_keyword'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'article_id' => Yii::t('app', 'Article ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'short_description' => Yii::t('app', 'Short Description'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'meta_keyword' => Yii::t('app', 'Meta Keyword'),
        ];
    }
}

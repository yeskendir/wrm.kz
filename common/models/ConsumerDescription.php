<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "consumer_description".
 *
 * @property int $id
 * @property int $consumer_id
 * @property int $language_id
 * @property string $title
 * @property string $description
 * @property string $short_description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 */
class ConsumerDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consumer_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['language_id', 'title', 'description', 'short_description', 'meta_title', 'meta_description', 'meta_keyword'], 'required'],
            [['language_id','consumer_id'], 'integer'],
            [['description'], 'string'],
            [['title', 'short_description', 'meta_title', 'meta_description', 'meta_keyword'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'consumer_id' => Yii::t('app', 'Consumer ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'short_description' => Yii::t('app', 'Short Description'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'meta_keyword' => Yii::t('app', 'Meta Keyword'),
        ];
    }
}
